# Cree Policy Analyzer
---
NOTE: To View this file please open it with a markdown viewer, or paste the contents in to https://pandao.github.io/editor.md/en.html

---

The purpose of this tool is to analyze ATRBAC-Safety policy files written in the Mohawk+T format.

The type of analysis this tool is able to perform is varied, currently we have the following statistical analyses:

 * Collecting statistical information from policy files (number of roles, number of each type of rules, ..., etc.)
 * Performing Cree's "Quick Decisions" on a policy to determine if a result can be found using the deterministic polynomial time algorithms
 * Performing Cree's "Static Slicing" techniques and comparing policy statistics (before vs after).
 * Performing Cree's "Abstraction Refinement" technique and comparing time/memory/cpu utilization/etc to running a solver without abstraction refinement
 * Comparing Cree's "Abstraction Refinement" technique and comparing it's effectiveness with each solving tool.
 * Comparing Cree's two model checking modes: Bounded Model Checking (BMC) and Symbolic Model Checking (SMC).

# Table of Contents

[TOC]

# Using the Tool

## Compiling
We suggest you use Eclipse version Oxygen.3a Release (4.7.3a), or better, to compile and run this tool.

Eclipse is an open source tool and free to use, it can be downloaded here: [https://www.eclipse.org/downloads/](https://www.eclipse.org/downloads/).


## Code Organization
This section is outlining the different interfaces that are available to use.

We currently have 3 interfaces that can be used:

 1. `cree.analyzer.CreePolicyAnalyzer` -- This class has a `public static void main(String[] args)` method and it's purpose is if you wish to interact using a single command to run a test.
 1. `cree.analyzer.CreePolicyAnalyzerCUI` -- This class has a `public static void main(String[] args)` method and it provides a Console User Interface (CUI) with convenient commands, the help menu printed, and a simple method for entering commands. This is an interactive method, and it what was mainly used during development.
 1. `cree.analyzer.CreePolicyAnalyzerInstance` -- This class can be imported into your own java project, you can then create as many instances as you want (be warned that log and results files should be separate if multiple instances are required). You can then run the instance with the following:

```java
 CreePolicyAnalyzerInstance inst = new CreePolicyAnalyzerInstance();
 String[] args = new String[] {/* command line options here */};
 inst.run(args);
```

## Command Line Options (CLOs)

<dl>
  <dt><b>--settings [filepath]</b></dt>
  <dd>This CLO allows for a file to be passed in that contains other CLOs. The file will be parsedd first, and any other CLOs included on the command line will overwrite the settings in the settings file.
  <br/>
  The settings file is a Property file:

```property
  results=results/my_results_1.csv
  log=logs/log_1.csv
  name=My Simple Test
  comment=This is a simple example of a settings file
  // This is a comment
  bulk=true
  policyFile=data/mohawkT/regression/
```
  </dd>
</dl>


# Results
This project stores its results in a SQLite3 database file.

This format was chosen because it provides many additional features over CSV/XLSX files that are useful for data scientists looking to analysis the data.

## SQLite3 Database Viewers
Here are some suggested SQLite3 viewers:

 * [SQLite Studio](https://sqlitestudio.pl/index.rvt) (we used this during development)
 * [DB Browser for SQLite](https://sqlitebrowser.org/)
 * [SQLite Online](https://sqliteonline.com/) (Online viewer in the browser)

## SQLite3 Views
In SQL there is the concept of a "View", where a search (SELECT statement) can be saved into a database and further searches can be made against that subset of data.

We have use Views to document the data used in the paper.

## Organization
Each type of results are stored in their own separate table; this is to better organize results and to prevent wasted space for test files that have not had a specific test performed on it.

## Table PolicyStats
This table contains all of the results when obtaining statistics about Mohawk+T policy files.

Below is only a subset of the columns stored in the the PolicyStats table.
<br/>
Please see the `create-results-PolicyStats-table.sql` file for descriptions about each column.
<br/>
Or you can run the SQL command `create-results-PolicyStats-column-description-table.sql`, which will create a new table in your SQLite database with descriptions for each column (I prefer this method).


<dl>
<dt><b>id	INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL</b></dt>
<dd>Unique row id</dd>
<dt><b>batch_id	INTEGER NOT NULL</b></dt>
<dd>When the policy analyzer is performing a BULK analysis on a folder of policy files, each new row will have the same batch_id.</dd>
<dt><b>date_created	DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP)</b></dt>
<dd>The date that this row is created (set automatically upon inserting into the table)</dd>
<dt><b>last_updated	DATETIME NOT NULL DEFAULT (CURRENT_TIMESTAMP)</b></dt>
<dd>This field is initialized to date_created, but if the policy editor updates unset values, it will update this field to indicate that the row has been updated.</dd>
<dt><b>created_by	VARCHAR(80) NOT NULL</b></dt>
<dd>The name of the program which entered this row into the database (can be retroactively updated).</dd>
<dt><b>filename	VARCHAR(256)</b></dt>
<dd>The policy file's name (does not include the file path). For in-memeory policies a special name can be used to indicate it.</dd>
<dt><b>filepath	TEXT</b></dt>
<dd>The complete folder path to the policy file. Can be NULL if the policy is in-memory.</dd>
<dt><b>file_sha256	VARCHAR(257)</b></dt>
<dd>The SHA256 of the file. In-memory policies can get teh text version and SHA256 that version.</dd>
<dt><b>result_expected	CHAR</b></dt>
<dd>If the policy file comes with a "Expected" tag, then the result is stored here. The possible values are: 'R' (reachable), 'U'( unreachable), NULL (not present)</dd>
<dt><b>result_cree	CHAR</b></dt>
<dd>The result returned by the program Cree. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)</dd>
<dt><b>result_cree_dt	DATETIME</b></dt>
<dd>The date and time when this policy was run through the Cree program.</dd>
<dt><b>result_mohawk_t	CHAR</b></dt>
<dd>The result returned by the program Mohawk+T. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)</dd>
<dt><b>result_mohawk_t_dt	DATETIME</b></dt>
<dd>The date and time when this policy was run through the Mohawk+T program.</dd>
<dt><b>result_asasptime_nsa	CHAR</b></dt>
<dd>The result returned by the program ASASPTime NSA. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)</dd>
<dt><b>result_asasptime_nsa_dt	DATETIME</b></dt>
<dd>The date and time when this policy was run through the ASASPTime NSA program.</dd>
<dt><b>result_asasptime_sa	CHAR</b></dt>
<dd>The result returned by the program ASASPTime SA. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)</dd>
<dt><b>result_asasptime_sa_dt	DATETIME</b></dt>
<dd>The date and time when this policy was run through the ASASPTime SA program.</dd>
<dt><b>result_uzun_tredrole	CHAR</b></dt>
<dd>The result returned by the program TREDRole. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)</dd>
<dt><b>result_uzun_tredrole_dt	DATETIME</b></dt>
<dd>The date and time when this policy was run through the TREDRole program.</dd>
<dt><b>num_goal_roles	INTEGER</b></dt>
<dd>The number of goal roles in the policy</dd>
<dt><b>num_all_rules	INTEGER</b></dt>
<dd>The number of all rules in the policy</dd>
<dt><b>num_all_timeslots_non_overlapping	INTEGER</b></dt>
<dd>The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping </dd>
<dt><b>num_all_timeintervals_admin	INTEGER</b></dt>
<dd>The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals</dd>
<dt><b>num_all_timeslots_target	INTEGER</b></dt>
<dd>The number of unique time slots that appear in the target time slot array for all rules in the policy</dd>
<dt><b>num_all_rules_admin_true	INTEGER</b></dt>
<dd>The number of rules that have TRUE for the admin condition in the whole policy</dd>
<dt><b>num_all_rules_precond_true	INTEGER</b></dt>
<dd>The number of rules in the policy where the pre-condition is set to TRUE.</dd>
<dt><b>num_all_rules_target_goal	INTEGER</b></dt>
<dd>The number of rules in the policy where the target role is also one of the goal roles</dd>
<dt><b>num_all_rules_precond_goal	INTEGER</b></dt>
<dd>The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles</dd>
<dt><b>num_all_rules_admin_goal	INTEGER</b></dt>
<dd>The number of rules in the policy where the admin condition is one of the goal roles.</dd>
<dt><b>num_all_roles	INTEGER</b></dt>
<dd>The number of roles used in the entire policy (counts over: goal, target, admin, and pre-condition roles)</dd>
<dt><b>num_all_roles_admin	INTEGER</b></dt>
<dd>The number of roles that appear in the admin condition of all rules in the policy</dd>
<dt><b>num_all_roles_pos_cond	INTEGER</b></dt>
<dd>The number of roles that appear as positive conditions in all rules in the policy</dd>
<dt><b>num_all_roles_neg_cond	INTEGER</b></dt>
<dd>The number of roles that appear as negative conditions in all rules in the policy</dd>
<dt><b>num_all_roles_cond	INTEGER</b></dt>
<dd>The number of roles that appear in the pre-condition in all rules in the policy</dd>
<dt><b>num_all_roles_longest_cond	INTEGER</b></dt>
<dd>The number of roles in the longest pre-conditiion in all rules in the policy</dd>
<dt><b>num_all_roles_longest_pos_cond	INTEGER</b></dt>
<dd>The largest number of positive roles in pre-conditions in all rules in the policy</dd>
<dt><b>num_all_timeslots_longest_tsarray	INTEGER</b></dt>
<dd>The largest number of timeslots in target timeslot array in all rules in the policy</dd>
<dt><b>num_all_rules_longest_cond	INTEGER</b></dt>
<dd>The number of rules in the policy that contain a pre-condition of length equal to 'num_all_roles_longest_cond' </dd>
<dt><b>num_all_rules_longest_pos_cond	INTEGER</b></dt>
<dd>The number of rules in the policy that contain the same number of positive roles in the pre-condition of llength equal to 'num_all_roles_longest_pos_cond' </dd>
<dt><b>num_all_rules_longest_tsarray	INTEGER</b></dt>
<dd>The number of rules in the policy that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' </dd>
<dt><b>num_ca_rules	INTEGER</b></dt>
<dd>The number of all rules in the Can Assign rule section</dd>
<dt><b>num_ca_rules_percent	REAL</b></dt>
<dd>The percentage of rules that are Can Assign rules compared to the total number of rules [0.0, 100.0]</dd>
<dt>...</dt>
<dd>All the same ints from num_all_* but for CA, CR, CE, and CD</dd>
</dl>