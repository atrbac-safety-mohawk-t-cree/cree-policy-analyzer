import os
import psutil
import time
import datetime
import traceback
 
pids = set()
for pid in psutil.pids():
    pids.add(pid)

logPath = r'procLog(%s).csv' % datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
with open(logPath, 'w') as f:
    f.write("DateTime,Time(sec),Name,Status,CPU Affinity,CPU User,CPU System,%MEM,MEM RSS, MEM VMS,Create Time\n")
while 1:
    for pid in psutil.pids():
        try:
            if pid in pids:
                # skip already seen pid
                continue
            proc = psutil.Process(pid)
            print(proc)
            cpu = proc.cpu_times()
            mem = proc.memory_info()
            d = {
                'cpu_user': cpu.user,
                'cpu_system': cpu.system,
                'cpu_affinity': len(proc.cpu_affinity()),
                'mem_percent': proc.memory_percent(),
                'status': proc.status(),
                'created_time': time.ctime(proc.create_time()),
                'time': time.ctime(),
                'time_sec': time.time(),
                'rss': mem.rss,
                'vms': mem.vms,
                'name': proc.name()
            }
            with open(logPath, 'a') as f:
                f.write('{time},{time_sec},{name},{status},{cpu_affinity},{cpu_user},{cpu_system},{mem_percent},{rss},{vms},{created_time}\n'.format(**d))
        except psutil._exceptions.AccessDenied as e:
            print('Skipping proc with pid:', pid)
            continue
        except psutil._exceptions.NoSuchProcess as e:
            # print('Process has ended with pid:', pid)
            continue
        except Exception as e:
            print('Got a weird Error:', e)
            traceback.print_exc()
    print("Finished log update!")
    time.sleep(1)
    print("writing new log data!")