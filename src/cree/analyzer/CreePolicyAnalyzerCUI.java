package cree.analyzer;

import java.io.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class CreePolicyAnalyzerCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "CreePolicyAnalyzerCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] ignored) {
        CreePolicyAnalyzerInstance inst = new CreePolicyAnalyzerInstance();
        ArrayList<String> argv = new ArrayList<String>();
        ArrayList<String[]> cmds = new ArrayList<String[]>();
        String arg = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.println("To run stats on all test cases: !all_stats");
        System.out.println("To run slicing on all test cases: !all_slicing");
        System.out.println("To run abstraction refinement on all test cases: !all_absref");
        System.out.print("Enter Commandline Argument ('!e' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            arg = user_input.next();
            fullCommand.append(arg + " ");

            if (quotedStr.isEmpty() && arg.startsWith("\"")) {
                System.out.println("Starting: " + arg);
                quotedStr = arg.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && arg.endsWith("\"")) {
                System.out.println("Ending: " + arg);
                argv.add(quotedStr + " " + arg.substring(0, arg.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + arg;
                continue;
            }

            if (arg.equals("!e")) {
                cmds.add(argv.toArray(new String[1]));
                break;
            }

            if (arg.equals("!p")) {
                argv.clear();
                cmds.add(previousCmd.split(" "));
                break;
            }

            if (arg.equals("!all_stats")) {
                argv.clear();
                cmds = allStats();
                break;
            }
            if (arg.equals("!all_slicing")) {
                argv.clear();
                cmds = allSlicing();
                break;
            }
            if (arg.equals("!all_absref")) {
                argv.clear();
                cmds = allAbsRef();
                break;
            }

            argv.add(arg);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        if (!arg.equals("!p") && !arg.equals("!all_stats")) {
            try {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            } catch (IOException e) {
                System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }
        }

        long fullstart = System.currentTimeMillis();
        int count = 1;
        for (String[] c : cmds) {
            System.out.println("==============================================================");
            System.out.println("== CMD " + count + "/" + cmds.size() + ": " + getCMDString(c));
            System.out.println("==================START OF OUTPUT=============================");
            long start = System.currentTimeMillis();
            inst = new CreePolicyAnalyzerInstance();
            inst.run(c);
            Duration d = Duration.ofMillis(System.currentTimeMillis() - start);
            System.out.println("====================END OF OUTPUT============================");
            System.out.println("Done [" + humanReadableFormat(d) + "]");
            System.out.println("");

            count++;
        }

        if (cmds.size() > 1) {
            Duration d = Duration.ofMillis(System.currentTimeMillis() - fullstart);
            System.out.println("\n");
            System.out.println("===========================");
            System.out.println("===========================");
            System.out.println("Done all Tests [" + humanReadableFormat(d) + "]");
        }
    }

    /**
     * Create a string that can be copied and pasted into the CUI for quickly running a command
     * @param cmds
     * @return
     */
    private static String getCMDString(String[] cmds) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String s : cmds) {
            if (first) {
                first = false;
            } else {
                sb.append(" ");
            }
            if (s.contains(" ")) {
                sb.append("\"").append(s).append("\"");
            } else {
                sb.append(s);
            }
        }
        return sb.toString();
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    public static ArrayList<String[]> allStats() {
        ArrayList<String[]> cmds = new ArrayList<>();
        String stats = "-run stats -loglevel quiet ";
        // Regression Tests
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/reachable").split(" "));
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/unreachable").split(" "));

        // From original Mohawk paper
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/positive").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixednocr").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixed").split(" "));

        // Rainse Testcases
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuiteb/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/hos/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/univ/").split(" "));

        // Uzun Testcases
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/roles/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/rules/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/timeslots/").split(" "));

        // Shahen Testcases
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v1/").split(" "));
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v2/").split(" "));

        return cmds;
    }
    public static ArrayList<String[]> allSlicing() {
        ArrayList<String[]> cmds = new ArrayList<>();
        String stats = "-run slicing -loglevel quiet ";
        // Regression Tests
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/reachable").split(" "));
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/unreachable").split(" "));

        // From original Mohawk paper
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/positive").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixednocr").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixed").split(" "));

        // Rainse Testcases
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuiteb/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/hos/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/univ/").split(" "));

        // Uzun Testcases
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/roles/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/rules/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/timeslots/").split(" "));

        // Shahen Testcases
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v1/ -db results/shahen.sqlite3").split(" "));
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v2/ -db results/shahen.sqlite3").split(" "));

        return cmds;
    }
    public static ArrayList<String[]> allAbsRef() {
        ArrayList<String[]> cmds = new ArrayList<>();
        String stats = "-run absref -loglevel quiet ";
        // Regression Tests
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/reachable").split(" "));
        cmds.add((stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/unreachable").split(" "));

        // From original Mohawk paper
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/positive").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixednocr").split(" "));
        cmds.add((stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixed").split(" "));

        // Rainse Testcases
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuiteb/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/hos/").split(" "));
        cmds.add((stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/univ/").split(" "));

        // Uzun Testcases
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/roles/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/rules/").split(" "));
        cmds.add((stats + "-bulk -owner uzun -input data/mohawkT/uzun/timeslots/").split(" "));

        // Shahen Testcases
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v1/ -db results/shahen.sqlite3 ").split(" "));
        cmds.add((stats + "-bulk -owner shahen -input data/mohawkT/shahen/v2/ -db results/shahen.sqlite3").split(" "));
        return cmds;
    }

    public static void printCommonCommands() {
        String stats = "-run stats -loglevel quiet ";
        System.out.println("\n\n--- Common Commands ---");
        System.out.println("\n#########################STATS#####################################");
        System.out.println("                    Regression Tests");
        System.out.println(stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/reachable !e");
        System.out.println(stats + "-bulk -owner shahen_regression -input data/mohawkT/regression/unreachable !e");
        System.out.println("");
        System.out.println("                    From original Mohawk paper");
        System.out.println(stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/positive !e");
        System.out.println(stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixednocr !e");
        System.out.println(stats + "-bulk -owner jayaraman -input data/mohawkT/mohawk/mixed !e");
        System.out.println("");
        System.out.println("                    Rainse Testcases");
        System.out.println(stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuiteb/ !e");
        System.out.println(stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/hos/ !e");
        System.out.println(stats + "-bulk -owner ranise -input data/mohawkT/ranise/testsuitec/univ/ !e");
        System.out.println("");
        System.out.println("                    Uzun Testcases");
        System.out.println(stats + "-bulk -owner uzun -input data/mohawkT/uzun/roles/ !e");
        System.out.println(stats + "-bulk -owner uzun -input data/mohawkT/uzun/rules/ !e");
        System.out.println(stats + "-bulk -owner uzun -input data/mohawkT/uzun/timeslots/ !e");
        System.out.println("");
        System.out.println("                    Shahen Testcases");
        System.out.println(stats + "-bulk -owner shahen -input data/mohawkT/shahen/v1/ -db results/shahen.sqlite3 !e");
        System.out.println(stats + "-bulk -owner shahen -input data/mohawkT/shahen/v2/ -db results/shahen.sqlite3 !e");

        System.out.println("\n#########################DEBUG#####################################");
        System.out.println(stats + " -owner shahen_regression "
                + "-input data/mohawkT/regression/reachable/reachable-test01.mohawkT !e");
        System.out.println(stats + " -owner jayaraman -input data/mohawkT/mohawk/positive/pos-test01.mohawkT !e");
        System.out.println("");
        System.out.println("---");
        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command: " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
