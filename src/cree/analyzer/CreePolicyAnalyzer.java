package cree.analyzer;

import java.util.logging.Logger;

public class CreePolicyAnalyzer {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        CreePolicyAnalyzerInstance inst = new CreePolicyAnalyzerInstance();

        inst.run(args);
    }

}
