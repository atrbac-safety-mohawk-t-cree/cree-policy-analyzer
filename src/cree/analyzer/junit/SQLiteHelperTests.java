package cree.analyzer.junit;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import cree.analyzer.helper.SQLiteHelper;
import cree.analyzer.results.PolicyStatistics;
import cree.analyzer.results.PolicyStatisticsManager;

class SQLiteHelperTests {
    public static String dbFile = "tests/testdb.db";
    public static String table = "PolicyStats";
    public static String resourceTable = "file/PolicyStats_Schema.txt";
    public static String createPolicyStatsSQL = "file/PolicyStats-table.sql";
    public static String createPolicyStatsDescriptionSQL = "file/PolicyStats-column-description-table.sql";

    public static SQLiteHelper helper;
    public static PolicyStatisticsManager psm;

    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        helper = new SQLiteHelper(new File(dbFile), table);
        assertTrue(helper.connect());
        assertTrue(helper.checkTableExists());

        psm = new PolicyStatisticsManager(helper);
    }

    @Test
    void verifyTable() {
        assertTrue(helper.verifyTableSchema(resourceTable));
    }

    @Test
    void sqlQueries() throws Exception {
        PolicyStatistics psr = new PolicyStatistics("FAKE SHA");
        String query;

        query = psm.getSQLInsert(psr);
        System.out.println("SQL Statement: " + query);
        assertTrue(query.startsWith("INSERT"));

        psr.batch_id = 12;
        query = psm.getSQLInsert(psr);
        System.out.println("SQL Statement: " + query);
        assertTrue(query.contains("12"));

        psr.id = 1;
        query = psm.getSQLInsert(psr);
        System.out.println("SQL Statement: " + query);
        assertTrue(query.startsWith("UPDATE"));
    }

    @Test
    void largestInt() throws Exception {
        assertEquals(new Integer(99), helper.getLargestInt("batch_id"));
    }
}
