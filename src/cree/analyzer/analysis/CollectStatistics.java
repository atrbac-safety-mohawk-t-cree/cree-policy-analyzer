package cree.analyzer.analysis;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

import cree.analyzer.helper.SQLiteHelper;
import cree.analyzer.results.*;
import cree.helper.CreeTestingSuite;
import cree.optimization.boundestimation.BoundEstimation;
import cree.optimization.heuristics.MohawkTHeuristics;
import mohawk.global.helper.ParserHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.*;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.*;
import mohawk.global.timing.MohawkTiming;

public class CollectStatistics {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String created_by = "Cree Policy Analyzer (v1.2)";
    public SQLiteHelper helper;
    public PolicyStatisticsManager psm;
    public PolicyStatistics ps;
    public MohawkT m;
    public CreeTestingSuite tests;
    public MohawkTiming timing;

    HashMap<Role, HashSet<TimeSlot>> assignableRoles;
    HashMap<Role, HashSet<TimeSlot>> enableableRoles;

    public CollectStatistics(MohawkTiming t, SQLiteHelper h, MohawkT m, String sha256) throws SQLException {
        timing = t;
        helper = h;
        boolean b = helper.connect();
        if (b == false) { throw new IllegalArgumentException("Unable to connect to the database"); }

        psm = new PolicyStatisticsManager(helper);
        this.m = m;
        ps = new PolicyStatistics(sha256);
    }

    /**
     * Loads the internal {@link PolicyStatistics} with results stored in the database, if they exists
     * @throws SQLException
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    public void getPreviousResults() throws SQLException, IllegalArgumentException, IllegalAccessException {
        psm.getExistingResults(ps, true);
    }

    public void collectAllStatistics()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        ps.num_goal_roles = m.query._roles.size();
        ps.num_all_rules = m.numberOfRules();
        ps.num_all_roles = m.numberOfRoles();
        ps.num_all_timeslots = m.numberOfTimeslots();

        ps.num_ca_rules = m.canAssign.size();
        ps.num_cr_rules = m.canRevoke.size();
        ps.num_ce_rules = m.canEnable.size();
        ps.num_cd_rules = m.canDisable.size();

        assignableRoles = m.canAssign.getTargetRoleToTimeslots();
        enableableRoles = m.canEnable.getTargetRoleToTimeslots();

        // ALL needs to be first, so that the *_longest_* values can be set
        gatherRuleStats(m.getAllRules(), "num", "all");
        logger.fine(ps.toString());
        // Requires "all" being called first
        gatherRuleStats(m.canAssign.getRules(), "num", "ca");
        gatherRuleStats(m.canRevoke.getRules(), "num", "cr");
        gatherRuleStats(m.canEnable.getRules(), "num", "ce");
        gatherRuleStats(m.canDisable.getRules(), "num", "cd");
        logger.fine("After ca/cr/ce/cd was called");
        logger.fine(ps.toString());

        // Collect the longest values and add them up for the "all" category
        ps.num_all_longest_rules_cond = ps.num_ca_longest_rules_cond + ps.num_cr_longest_rules_cond
                + ps.num_ce_longest_rules_cond + ps.num_cd_longest_rules_cond;
        ps.num_all_longest_rules_pos_cond = ps.num_ca_longest_rules_pos_cond + ps.num_cr_longest_rules_pos_cond
                + ps.num_ce_longest_rules_pos_cond + ps.num_cd_longest_rules_pos_cond;
        ps.num_all_longest_rules_tsarray = ps.num_ca_longest_rules_tsarray + ps.num_cr_longest_rules_tsarray
                + ps.num_ce_longest_rules_tsarray + ps.num_cd_longest_rules_tsarray;

        ps.stat_size_max = ps.num_all_roles * ps.num_all_roles * ps.num_all_timeslots
                + ps.num_all_roles * ps.num_all_timeslots;
        // System.out.printf("stat_size_max: %d=%d*%d*%d + %d*%d\n", ps.stat_size_max, ps.num_all_roles,
        // ps.num_all_roles,
        // ps.num_all_timeslots, ps.num_all_roles, ps.num_all_timeslots);
        ps.stat_size_min = (ps.num_all_roles_admin + 1) * ps.num_ca_roles * ps.num_ca_timeslots_target
                + ps.num_ce_roles * ps.num_ce_timeslots_target;
        // System.out.printf("stat_size_min: %d=%d*%d*%d + %d*%d\n", ps.stat_size_min, (ps.num_all_roles_admin + 1),
        // ps.num_ca_roles, ps.num_all_timeslots, ps.num_ce_roles, ps.num_all_timeslots);

        BoundEstimation be = new BoundEstimation(m, m.policyFilename, timing, "CollectStats");
        ps.bound_estimation = (int) be.calculateBound();

        if (ps.num_all_rules != 0) {
            ps.stat_ca_rules_percent = 100.0 * (m.canAssign.size() / (double) ps.num_all_rules);
            ps.stat_cr_rules_percent = 100.0 * (m.canRevoke.size() / (double) ps.num_all_rules);
            ps.stat_ce_rules_percent = 100.0 * (m.canEnable.size() / (double) ps.num_all_rules);
            ps.stat_cd_rules_percent = 100.0 * (m.canDisable.size() / (double) ps.num_all_rules);
        } else {
            ps.stat_ca_rules_percent = 0.0;
            ps.stat_cr_rules_percent = 0.0;
            ps.stat_ce_rules_percent = 0.0;
            ps.stat_cd_rules_percent = 0.0;
        }

        if (ps.heuristics_description == null) {
            runHeuristics();
        }

        // TOOK TOO LONG
        // if (m.expectedResult != ExpectedResult.UNREACHABLE) {
        // CounterExample ce = getCounterExample();
        // collectPathStats(ce);
        // ps.path_raw = ce.ruleString();
        // }

        if (ps.path_raw != null) {
            MohawkT pathPolicy = pathToCounterExample(ps.path_raw);
            collectPathStats(pathPolicy);
        }
        logger.fine("After OTHER");
        logger.fine(ps.toString());
    }

    public static MohawkT pathToCounterExample(String path_raw) {
        if (path_raw.indexOf("->") == -1) { return null; }
        if (path_raw.endsWith("Goal State is Unreachable")) { return new MohawkT(); }

        StringBuilder ca = new StringBuilder();
        StringBuilder cr = new StringBuilder();
        StringBuilder ce = new StringBuilder();
        StringBuilder cd = new StringBuilder();

        ca.append("CanAssign{");
        cr.append("CanRevoke{");
        ce.append("CanEnable{");
        cd.append("CanDisable{");

        for (String s : path_raw.split(" -> ")) {
            String[] ss = s.split(":");
            if (ss.length != 2) {
                // Skip the first and last states
                continue;
            }

            switch (ss[0]) {
                case "CanAssign" :
                    ca.append(ss[1]);
                    break;
                case "CanRevoke" :
                    cr.append(ss[1]);
                    break;
                case "CanEnable" :
                    ce.append(ss[1]);
                    break;
                case "CanDisable" :
                    cd.append(ss[1]);
                    break;
                default :
                    return null;
            }
        }
        ca.append("}");
        cr.append("}");
        ce.append("}");
        cd.append("}");

        String policyStr = "Query: t1, [r2] Expected: UNKNOWN " + ca.toString() + cr.toString() + ce.toString()
                + cd.toString();

        ParserHelper parserHelper = new ParserHelper();
        MohawkTARBACParser parser;
        try {
            parser = parserHelper.parseMohawkTString(policyStr);

            if (parserHelper.error.errorFound) { return null; }
            return parser.mohawkT;
        } catch (IOException e) {
            return null;
        }
    }

    private void collectPathStats(MohawkT pathPolicy)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {

        if (pathPolicy == null) { return; }
        // System.out.println("Path Policy: " + pathPolicy);

        // ALL needs to be first, so that the *_longest_* values can be set
        gatherRuleStats(pathPolicy.getAllRules(), "path", "all");
        // Requires "all" being called first
        gatherRuleStats(pathPolicy.canAssign.getRules(), "path", "ca");
        gatherRuleStats(pathPolicy.canRevoke.getRules(), "path", "cr");
        gatherRuleStats(pathPolicy.canEnable.getRules(), "path", "ce");
        gatherRuleStats(pathPolicy.canDisable.getRules(), "path", "cd");

        // Collect the longest values and add them up for the "all" category
        ps.path_all_longest_rules_cond = ps.num_ca_longest_rules_cond + ps.num_cr_longest_rules_cond
                + ps.num_ce_longest_rules_cond + ps.num_cd_longest_rules_cond;
        ps.path_all_longest_rules_pos_cond = ps.num_ca_longest_rules_pos_cond + ps.num_cr_longest_rules_pos_cond
                + ps.num_ce_longest_rules_pos_cond + ps.num_cd_longest_rules_pos_cond;
        ps.path_all_longest_rules_tsarray = ps.num_ca_longest_rules_tsarray + ps.num_cr_longest_rules_tsarray
                + ps.num_ce_longest_rules_tsarray + ps.num_cd_longest_rules_tsarray;
    }

    private void runHeuristics() {
        long startTime = System.currentTimeMillis();
        MohawkTHeuristics heuristics = new MohawkTHeuristics(m);
        heuristics.debug = false;
        ExecutionResult result = heuristics.getResult();
        long duration = System.currentTimeMillis() - startTime;

        ps.heuristics_result = result.name();
        ps.heuristics_duration = (int) duration;
        ps.heuristics_description = heuristics.reason;
    }

    public CounterExample getCounterExample() {
        MohawkTiming timing = new MohawkTiming();
        MohawkResults results = new MohawkResults();
        tests = new CreeTestingSuite(null, results, null, timing, "programs/NuSMV-2.6.0-win64/bin/NuSMV.exe");

        if (ps.stat_size_min >= 1000) {
            tests.mode = NuSMVMode.SMC;
        } else {
            tests.mode = NuSMVMode.BMC;
        }

        CounterExample ce = null;
        try {
            tests.runTest(m, "Cree Policy Analyzer", "", "", false, false, false, false, null);
            ce = tests.lastCounterExample;
            // System.out.println(ce);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ce;
    }

    private PolicyStatisticsBlock gatherRuleStats(ArrayList<Rule> rules, String prefix, String key)
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        PolicyStatisticsBlock psb = new PolicyStatisticsBlock();
        /** The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 
         * 3 time-intervals */
        HashSet<TimeInterval> set_timeintervals_admin = new HashSet<>();
        /** The number of unique time slots that appear in the target time slot array for all rules in the 
         * Can Assign rule section */
        HashSet<TimeSlot> set_timeslots_target = new HashSet<>();
        /** The number of roles used in the entire Can Assign rule section (counts over: target, admin, 
         * and pre-condition roles) */
        HashSet<Role> set_roles = new HashSet<>();
        /** The number of roles that appear in the admin condition of all rules in the Can Block rule section */
        HashSet<Role> set_roles_admin = new HashSet<>();
        /** The number of roles that appear in the target role of all rules in the Can Block rule section */
        HashSet<Role> set_roles_target = new HashSet<>();
        /** The number of roles that appear as positive conditions in all rules in the Can Block rule section */
        HashSet<Role> set_roles_pos_cond = new HashSet<>();
        /** The number of roles that appear as negative conditions in all rules in the Can Block rule section */
        HashSet<Role> set_roles_neg_cond = new HashSet<>();
        /** */
        ArrayList<Double> preconditionSizes = new ArrayList<Double>(rules.size());
        /** */
        ArrayList<Double> tsarraySizes = new ArrayList<Double>(rules.size());

        TimeIntervalHelper tih = new TimeIntervalHelper();
        HashSet<Role> goalRoles = new HashSet<>();
        goalRoles.addAll(m.query._roles);
        ////////////////////////////////////////////////////////////////////////////

        for (Rule r : rules) {
            ////////////////////////////////////////////////////////////////////////////
            // Counting Longest values
            if (r._preconditions.size() > psb.num_all_longest_roles_cond) {
                psb.num_all_longest_roles_cond = r._preconditions.size();
            }
            if (r.getNumberOfPositiveRoles() > psb.num_all_longest_roles_pos_cond) {
                psb.num_all_longest_roles_pos_cond = r.getNumberOfPositiveRoles();
            }
            if (r.getNumberOfNegativeRoles() > psb.num_all_longest_roles_neg_cond) {
                psb.num_all_longest_roles_neg_cond = r.getNumberOfNegativeRoles();
            }
            if (r.numberOfConditions() > 0) {
                if (r.getNumberOfPositiveRoles() == r.numberOfConditions()) {
                    psb.num_all_rules_precond_pos += 1;
                } else if (r.getNumberOfPositiveRoles() == 0) {
                    psb.num_all_rules_precond_neg += 1;
                } else {
                    psb.num_all_rules_precond_mixed += 1;
                }
            }
            if (r._roleSchedule.size() > psb.num_all_longest_timeslots_tsarray) {
                psb.num_all_longest_timeslots_tsarray = r._roleSchedule.size();
            }
            if (!key.equals("all")) {
                if (prefix.equals("num")) {
                    if (r._preconditions.size() == ps.num_all_longest_roles_cond) {
                        psb.num_all_longest_rules_cond += 1;
                    }
                    if (r.getNumberOfPositiveRoles() == ps.num_all_longest_roles_pos_cond) {
                        psb.num_all_longest_rules_pos_cond += 1;
                    }
                    if (r._roleSchedule.size() == ps.num_all_longest_timeslots_tsarray) {
                        psb.num_all_longest_rules_tsarray += 1;
                    }
                } else if (prefix.equals("path")) {
                    if (r._preconditions.size() == ps.path_all_longest_roles_cond) {
                        psb.num_all_longest_rules_cond += 1;
                    }
                    if (r.getNumberOfPositiveRoles() == ps.path_all_longest_roles_pos_cond) {
                        psb.num_all_longest_rules_pos_cond += 1;
                    }
                    if (r._roleSchedule.size() == ps.path_all_longest_timeslots_tsarray) {
                        psb.num_all_longest_rules_tsarray += 1;
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // Counting Properties of Rules
            if (r.checkIsRuleStartable()) {
                psb.num_all_rules_startable += 1;
            }
            if (r.checkIsRuleTrulyStartable()) {
                psb.num_all_rules_truely_startable += 1;
            }
            if (r.isInvokablePrecondition()) {
                psb.num_all_rules_invokable += 1;
            }
            if (r._type == RuleType.ASSIGN || r._type == RuleType.REVOKE) {
                if (r.containsUnassignablePrecondition(assignableRoles)) {
                    psb.num_all_rules_unassignable_precond += 1;
                }
            } else {
                if (r.containsUnassignablePrecondition(enableableRoles)) {
                    psb.num_all_rules_unassignable_precond += 1;
                }
            }
            if (r.isAdminRoleTrue()) {
                psb.num_all_rules_admin_true += 1;
            }
            if (r.noConditions()) {
                psb.num_all_rules_precond_true += 1;
            }
            if (goalRoles.contains(r._role)) {
                psb.num_all_rules_target_goal += 1;
            }
            if (goalRoles.contains(r._adminRole)) {
                psb.num_all_rules_admin_goal += 1;
            }
            if (!Collections.disjoint(goalRoles, r._preconditions)) {
                psb.num_all_rules_precond_goal += 1;
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // SETS
            set_timeintervals_admin.add(r._adminTimeInterval);
            tih.add(r._adminTimeInterval);
            set_timeslots_target.addAll(r._roleSchedule);
            tih.addAll(r._roleSchedule);

            for (Role role : r._preconditions) {
                set_roles.add(role.toLimitedRole());
                if (role.isPositivePrecondition()) {
                    set_roles_pos_cond.add(role.toLimitedRole());
                } else {
                    set_roles_neg_cond.add(role.toLimitedRole());
                }
            }
            set_roles.add(r._role.toLimitedRole());
            set_roles_target.add(r._role.toLimitedRole());
            if (!r._adminRole.isAllRoles()) {
                set_roles.add(r._adminRole.toLimitedRole());
                set_roles_admin.add(r._adminRole.toLimitedRole());
            }
            ////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////
            // MEANs
            preconditionSizes.add(1.0 * r._preconditions.size());
            tsarraySizes.add(1.0 * r._roleSchedule.size());
            ////////////////////////////////////////////////////////////////////////////
        }

        ////////////////////////////////////////////////////////////////////////////
        // System.out.println("Roles: " + set_roles);
        psb.num_all_rules = rules.size();
        psb.num_all_roles = set_roles.size();
        psb.num_all_roles_admin = set_roles_admin.size();
        psb.num_all_roles_target = set_roles_target.size();

        psb.num_all_roles_pos_cond = set_roles_pos_cond.size();
        psb.num_all_roles_neg_cond = set_roles_neg_cond.size();

        set_roles_pos_cond.addAll(set_roles_neg_cond);
        psb.num_all_roles_cond = set_roles_pos_cond.size();

        psb.num_all_timeslots_target = set_timeslots_target.size();
        psb.num_all_timeintervals_admin = set_timeintervals_admin.size();

        tih.reduceToTimeslots();
        psb.num_all_timeslots = tih.sizeReduced();
        ////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////
        if (prefix.equals("num")) {
            psb.stat_all_mean_cond = mean(preconditionSizes);
            psb.stat_all_stddevp_cond = stddevp(preconditionSizes, psb.stat_all_mean_cond);

            psb.stat_all_mean_tsarray = mean(tsarraySizes);
            psb.stat_all_stddevp_tsarray = stddevp(tsarraySizes, psb.stat_all_mean_tsarray);
        }
        ////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////
        // Set the PolicyStatistics variable
        // /*DEBUG*/ System.out.println(psb);
        ps.importBlock(psb, prefix, key);
        return psb;
    }

    /**
     * Sets the values batch_id, filepath, and filename, but only if they are already NULL
     * @param batch_id can be NULL
     * @param owner 
     */
    public void setMetaData(Integer batch_id, String owner) {
        ps.batch_id = batch_id;
        ps.filepath = m.policyFilename;
        ps.filename = new File(m.policyFilename).getName();
        ps.folder = new File(m.policyFilename).getParentFile().getName();
        ps.created_by = created_by;
        ps.owner = owner;
        ps.last_updated = new Timestamp(System.currentTimeMillis());
        ps.result_expected = m.expectedResult.name();
    }

    public void setAlteration(String method, Long duration) {
        ps.alteration_method = method;
        ps.alteration_duration = duration.intValue();
    }

    /**
     * Returns the population variance in the specified array.
     *
     * @param  a the array
     * @return the population variance in the array {@code a[]};
     *         {@code Double.NaN} if no such value
     */
    public static double stddevp(ArrayList<Double> a, double mean) {
        if (a == null) throw new IllegalArgumentException("argument is null");
        if (a.size() == 0) return 0;
        double sum = 0.0;
        for (int i = 0; i < a.size(); i++) {
            sum += (a.get(i) - mean) * (a.get(i) - mean);
        }
        return Math.sqrt(sum / a.size());
    }
    /**
     * Returns the average value in the specified array.
     *
     * @param  a the array
     * @return the average value in the array {@code a[]};
     *         {@code Double.NaN} if no such value
     */
    public static double mean(ArrayList<Double> a) {
        if (a == null) throw new IllegalArgumentException("argument is null");
        if (a.size() == 0) return 0;
        double sum = 0;
        for (double aa : a) {
            sum += aa;
        }
        return sum / a.size();
    }
}
