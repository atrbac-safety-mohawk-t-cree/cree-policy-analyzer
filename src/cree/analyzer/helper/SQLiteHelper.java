package cree.analyzer.helper;

import java.io.File;
import java.io.InputStream;
import java.sql.*;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * 
 * Useful Link: <a href="https://www.tutorialspoint.com/sqlite/sqlite_java.htm">SQLite - Java</a>
 * 
 * @author Jonathan Shahen
 *
 */
public class SQLiteHelper {
    public static final Logger logger = Logger.getLogger("mohawk");

    public Connection conn = null;
    public File dbFile = null;
    public String table = null;
    public String lastError = null;

    /**
     * 
     * @param dbFile
     * @param table
     */
    public SQLiteHelper(File dbFile, String table) {
        this.dbFile = dbFile;
        this.table = table;
    }

    /**
     * 
     * @return TRUE if connection was created (or already exists); FALSE if connection not created
     * @throws SQLException
     */
    public boolean connect() throws SQLException {
        boolean connected = false;
        String url = "jdbc:sqlite:" + dbFile.getAbsolutePath();

        if (conn != null) { return true; }

        conn = DriverManager.getConnection(url);
        if (conn != null) {
            // DatabaseMetaData meta = conn.getMetaData();
            // System.out.println("The driver name is " + meta.getDriverName());
            connected = true;
        }

        return connected;
    }

    /**
     * Closes the SQLite connection to the database;
     * This is safe to call at the end of operation.
     * Safe to call multiple times and if no connection was made.
     */
    public void close() {
        logger.info("Closing Database");
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {}
            conn = null;
        }
    }

    /**
     * Checks the SQL table on the Connection, with the name {@link SQLiteHelper#table}, has the same schema as
     * the saved resource {@code schemaResource}
     * 
     * @param schemaResource a string that links to the resource text file that stores the table schema
     * @return TRUE if the SQL table {@link SQLiteHelper#table} has the same schema as the 
     * saved resource {@code schemaResource};<br/>
     * NULL if an error occurred; <br/>
     * FALSE otherwise
     */
    public Boolean verifyTableSchema(String schemaResource) {
        String sqlTableSchema = getTableSchemaString();
        String savedTableSchema = getSavedTableSchema(schemaResource);

        if (sqlTableSchema == null || savedTableSchema == null) { return null; }

        // MAKE SURE TO REFRESH ECLIPSE SO THAT IT KNOWS WHEN A RESOURCE FILE CHANGES
        // System.out.println("Saved Table Schema:\n" + savedTableSchema.trim());
        // System.out.println("Read Table Schema:\n" + sqlTableSchema.trim());

        return savedTableSchema.trim().equals(sqlTableSchema.trim());
    }

    /**
     * Using the current connection, this function retrieves the schema definition using `PRAGMA table_info()`
     * and then converts each row into a string for comparison
     * @return the string version of `PRAGMA table_info(table)`; NULL if an error occurred 
     * @throws SQLException 
     */
    public String getTableSchemaString() {
        if (conn == null) {
            lastError = "No connection was found when attempting to call SQLiteHelper.getTableSchemaString()";
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("PRAGMA table_info(" + table + ");");

            while (rs.next()) {
                int cid = rs.getInt("cid");
                String name = rs.getString("name");
                String type = rs.getString("type");
                int notnull = rs.getInt("notnull");

                sb.append(cid).append(",").append(name).append(",").append(type).append(",").append(notnull)
                        .append("\n");
            }
        } catch (SQLException e) {

        } finally {
            try {
                rs.close();
            } catch (Exception e) {}

            try {
                stmt.close();
            } catch (Exception e) {}
        }
        return sb.toString();
    }

    /**
     * Using the current connection, this function checks if the {@link SQLiteHelper.table} exists in the database 
     * @return TRUE if the table exists; FALSE if the table does not exists; NULL if an error occurred 
     * @throws  
     */
    public Boolean checkTableExists() {
        if (conn == null) {
            lastError = "No connection was found when attempting to call SQLiteHelper.checkTableExists()";
            return null;
        }

        try {
            String query = "SELECT name FROM sqlite_master WHERE type='table' AND name=?;";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, table);
            ResultSet rs = pstmt.executeQuery();

            // Check if the ResultSet is empty
            if (!rs.isBeforeFirst()) { return false; }
            return true;

        } catch (SQLException r) {
            return null;
        }
    }

    public String getSavedTableSchema(String resourceFile) {
        StringBuilder result = new StringBuilder("");

        // Get file from resources folder
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(resourceFile);

        try (Scanner scanner = new Scanner(is)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        }

        return result.toString();

    }

    public Integer getLargestInt(String column) {
        if (conn == null) {
            lastError = "No connection was found when attempting to call SQLiteHelper.checkTableExists()";
            return null;
        }

        try {
            String query = "SELECT MAX(" + column + ") as b FROM PolicyStats;";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            // Check if the ResultSet is empty
            if (!rs.isBeforeFirst()) { return 0; }
            return rs.getInt("b");

        } catch (SQLException r) {
            return null;
        }
    }
}
