// This file was automatically created by `generate_sql_java_class.py`
package cree.analyzer.results;

import java.lang.reflect.Field;

public class PolicyStatisticsBlock {
	////////////////////////////////////////////////////////////////////////////////
	// The average length of precondition for all rules in the policy.
	public Double stat_all_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the policy.
	public Double stat_all_stddevp_cond;
	// The average length of time-slot array for all rules in the policy.
	public Double stat_all_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the policy.
	public Double stat_all_stddevp_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the policy
	public Integer num_all_rules = 0;
	// The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_all_timeslots = 0;
	// The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_all_roles = 0;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the policy
	public Integer num_all_roles_admin = 0;
	// The number of roles that appear in the target role of all rules in the policy
	public Integer num_all_roles_target = 0;
	// The number of roles that appear as positive conditions in all rules in the policy
	public Integer num_all_roles_pos_cond = 0;
	// The number of roles that appear as negative conditions in all rules in the policy
	public Integer num_all_roles_neg_cond = 0;
	// The number of roles that appear in the pre-condition in all rules in the policy
	public Integer num_all_roles_cond = 0;
	//===================================
	// The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_all_timeintervals_admin = 0;
	// The number of unique time slots that appear in the target time slot array for all rules in the policy
	public Integer num_all_timeslots_target = 0;
	//===================================
	// The number of rules that have TRUE for the admin condition in the policy
	public Integer num_all_rules_admin_true = 0;
	// The number of rules in the policy where the pre-condition is set to TRUE.
	public Integer num_all_rules_precond_true = 0;
	// The number of rules in the policy where the pre-condition is only positive roles.
	public Integer num_all_rules_precond_pos = 0;
	// The number of rules in the policy where the pre-condition is only negative roles.
	public Integer num_all_rules_precond_neg = 0;
	// The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
	public Integer num_all_rules_precond_mixed = 0;
	// The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_all_rules_precond_goal = 0;
	// The number of rules in the policy where the target role is also one of the goal roles
	public Integer num_all_rules_target_goal = 0;
	// The number of rules in the policy where the admin condition is one of the goal roles.
	public Integer num_all_rules_admin_goal = 0;
	//===================================
	// The number of rules in the policy that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_all_rules_startable = 0;
	// The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_all_rules_truely_startable = 0;
	// The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_all_rules_invokable = 0;
	// The number of rules in the policy that contain a positive precondition that is unassignable.
	public Integer num_all_rules_unassignable_precond = 0;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the policy
	public Integer num_all_longest_timeslots_tsarray = 0;
	// The number of roles in the longest pre-condition in all rules in the policy
	public Integer num_all_longest_roles_cond = 0;
	// The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
	public Integer num_all_longest_roles_pos_cond = 0;
	// The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)
	public Integer num_all_longest_roles_neg_cond = 0;
	//===================================
	// The number of rules in the policy that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
	public Integer num_all_longest_rules_cond = 0;
	// The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'num_all_roles_longest_pos_cond' 
	public Integer num_all_longest_rules_pos_cond = 0;
	// The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to 'num_all_timeslots_longest_tsarray'
	public Integer num_all_longest_rules_tsarray = 0;
	////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PolicyStatisticsBlock {");

        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.get(this) != null) {
                    sb.append(field.getName()).append("=").append(field.get(this)).append(", ");
                }
            } catch (Exception e) {}
        }
        sb.append("}");
        return sb.toString();
    }
}
