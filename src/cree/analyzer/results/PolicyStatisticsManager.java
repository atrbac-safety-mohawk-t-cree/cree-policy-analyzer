package cree.analyzer.results;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.logging.Logger;

import cree.analyzer.helper.SQLiteHelper;

/**
 * 
 * @author Jonathan Shahen
 *
 */
public class PolicyStatisticsManager {
    public static final Logger logger = Logger.getLogger("mohawk");
    public SQLiteHelper slh;

    public PolicyStatisticsManager(SQLiteHelper slh) {
        this.slh = slh;
    }

    /**
     * 
     * @return
     * @throws SQLException 
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    public String getSQLInsert(PolicyStatistics r)
            throws SQLException, IllegalArgumentException, IllegalAccessException {
        StringBuilder cols = new StringBuilder();
        StringBuilder vals = new StringBuilder();

        boolean insert = true;
        if (r.id != null) {
            insert = false;
        }

        for (Field field : r.getClass().getDeclaredFields()) {
            if (field.getName().equals("id")) {
                // skip id, if it is not null then we use an update statement,
                // if it is null then it does not get added to the list
                continue;
            }
            if (field.get(r) != null) {
                logger.fine(field.getName() + " - " + field.getType() + " - " + field.get(r));
                if (vals.length() == 0) {
                    if (insert) {
                        cols.append(field.getName());
                        vals.append("'").append(field.get(r)).append("'");
                    } else {
                        vals.append(field.getName()).append(" = '").append(field.get(r)).append("'");
                    }
                } else {
                    if (insert) {
                        cols.append(",").append(field.getName());
                        vals.append(", '").append(field.get(r)).append("'");
                    } else {
                        vals.append(", ").append(field.getName()).append("='").append(field.get(r)).append("'");
                    }
                }
            }
        }
        String query;
        if (r.id == null) {
            query = "INSERT INTO `" + slh.table + "` (" + cols.toString() + ") VALUES (" + vals.toString() + ");";
        } else {
            // NOTE: cannot use LIMIT in an UPDATE/DELETE clause unless SQLite is compiled with it
            query = "UPDATE `" + slh.table + "` SET " + vals.toString() + " WHERE id = '" + r.id + "';";
        }
        return query;
    }

    /**
     * Fills this result instance with any existing results that are stored in the SQL connection.
     * <br/>
     * 
     * @param helper
     * @param getFirst 
     * @return TRUE if successfully found an entry in the connection; FALSE if unable to find an entry
     * @throws SQLException 
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    public Boolean getExistingResults(PolicyStatistics r, boolean getFirst)
            throws SQLException, IllegalArgumentException, IllegalAccessException {
        // Get the row count
        PreparedStatement pstmt = slh.conn.prepareStatement(
                "SELECT COUNT(*) as c FROM " + slh.table + " WHERE file_sha256 = ? AND alteration_method = ?");
        pstmt.setString(1, r.file_sha256);
        pstmt.setString(2, r.alteration_method);
        ResultSet rs = pstmt.executeQuery();

        int rowcount = rs.getInt("c");
        rs.close();
        pstmt.close();

        // Error Checking
        if (getFirst == false && rowcount > 1) {
            throw new IndexOutOfBoundsException("Found " + rowcount + " rows with the SH256 value of " + r.file_sha256
                    + ". If you want to skip this error, either delete all but 1 row or "
                    + "use the option useFirst when calling this function.");
        }
        if (rowcount < 0) { throw new SQLException("The number of rows returned by a negative number: " + rowcount); }

        // No results found -- never tested this policy before
        if (rowcount == 0) { return false; }

        // Can only be that rowcount == 1
        if (rowcount != 1) {
            throw new IllegalAccessError(
                    "This should be impossible; " + "a change in the code must have allowed this bug to exists; "
                            + "Please report to this to the repo author.");
        }

        // Get all rows with the same SHA256 as stored in this PolicyStatisticResults instance
        pstmt = slh.conn
                .prepareStatement("SELECT * FROM " + slh.table + " WHERE file_sha256 = ? AND alteration_method = ?");
        pstmt.setString(1, r.file_sha256);
        pstmt.setString(2, r.alteration_method);
        rs = pstmt.executeQuery();

        Class<? extends PolicyStatistics> c = r.getClass();
        for (Field f : c.getDeclaredFields()) {
            if (f.getType() == Character.class) {
                f.set(r, getChar(rs, f.getName()));
            } else if (f.getType() == Timestamp.class) {
                f.set(r, rs.getTimestamp(f.getName()));
            } else if (f.getType() == String.class) {
                f.set(r, rs.getString(f.getName()));
            } else {
                f.set(r, rs.getObject(f.getName()));
            }
        }
        rs.close();
        pstmt.close();

        return true;
    }

    private Character getChar(ResultSet rs, String column) throws SQLException {
        String r = rs.getString("result_cree");
        if (r == null) { return null; }
        return r.charAt(0);
    }
}
