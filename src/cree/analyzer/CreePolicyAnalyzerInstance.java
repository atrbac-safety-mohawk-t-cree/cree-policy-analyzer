package cree.analyzer;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.*;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import cree.analyzer.analysis.CollectStatistics;
import cree.analyzer.helper.SQLiteHelper;
import cree.analyzer.results.PolicyStatistics;
import cree.analyzer.results.PolicyStatisticsManager;
import cree.optimization.abstractionrefinement.AbstractionRefinement;
import cree.optimization.slicing.StaticSlicer;
import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.helper.FileHelper;
import mohawk.global.helper.ParserHelper;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.Rule;
import mohawk.global.timing.MohawkTiming;

public class CreePolicyAnalyzerInstance {
    private final String VERSION = "v1.0";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private String Logger_filepath = "logs/Cree-Policy-Analyzer-Log.csv";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String timingFile = "logs/Cree-Policy-Analyzer-Timing.csv";

    // DB Settings
    public String dbFile = "results/CreePolicyAnalyzerResults-development.sqlite3";
    public static String table = "PolicyStats";
    public static String resourceTable = "file/PolicyStats_Schema.txt";
    public static String createPolicyStatsSQL = "file/PolicyStats-table.sql";
    public static String createPolicyStatsDescriptionSQL = "file/PolicyStats-column-description-table.sql";

    // Result Classes
    public SQLiteHelper helper;
    public PolicyStatisticsManager psm;

    // Helpers
    public MohawkTiming timing;
    public Vector<PolicyStatistics> results;
    public FileHelper fileHelper = new FileHelper();
    public ParserHelper parserHelper = new ParserHelper();

    // Settings
    public boolean debug = false;

    public int run(String[] args) {
        try {
            Integer batch_id = null;
            timing = new MohawkTiming();
            results = new Vector<PolicyStatistics>();

            ////////////////////////////////////////////////////////////////////////////////
            // COMMANDLINE OPTIONS
            CommandLine cmd = init(args);
            if (cmd == null) { return 0; }

            // Only set tests.debug when equal to TRUE (this allows for default to be changed to TRUE)
            if (debug) {
                timing.printOnStop = true;
            }
            // COMMANDLINE OPTIONS
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // Setup connection with the results database
            /* Timing */timing.startTimer("connectDB");
            helper = new SQLiteHelper(new File(dbFile), table);
            assertTrue(helper.connect());
            assertTrue(helper.checkTableExists());
            psm = new PolicyStatisticsManager(helper);
            /* Timing */timing.stopTimer("connectDB");
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // LOADING ALL Mohawk+T FILES
            /* Timing */timing.startTimer("loadFiles");
            fileHelper.loadSpecFiles();
            /* Timing */timing.stopTimer("loadFiles");
            // LOADING ALL Mohawk+T FILES
            ////////////////////////////////////////////////////////////////////////////////

            // Execute the test cases
            if (cmd.hasOption(CreePolicyAnalyzerOptionString.RUN.toString())) {
                String runVal = cmd.getOptionValue(CreePolicyAnalyzerOptionString.RUN.toString());
                logger.info("[ACTION] Run paramter detected: " + runVal);

                Integer numFiles = fileHelper.specFiles.size();

                ////////////////////////////////////////////////////////////////////////////////
                // INDIVIDUAL FILE LOOP
                logger.info("Mohawk+T Policy Files to Analyze: " + fileHelper.printFileNames(300, true));
                for (Integer i = 1; i <= fileHelper.specFiles.size(); i++) {
                    ////////////////////////////////////////////////////////////////////////////////
                    /* TIMING */String timerName = "mainLoop (" + i + "/" + numFiles + ")";
                    /* TIMING */timing.startTimer(timerName);
                    File specFile = fileHelper.specFiles.get(i - 1);
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // PARSING MOHAWK+T FILE
                    /* Timing */timing.startTimer(timerName + "-parseFile (" + i + ")");
                    logger.info("Processing File (" + i + "/" + numFiles + "): " + specFile.getAbsolutePath());
                    MohawkTARBACParser parser = parserHelper.parseMohawkTFile(specFile);
                    /* Timing */timing.stopTimer(timerName + "-parseFile (" + i + ")");

                    MohawkT m = parser.mohawkT;
                    m.policyFilename = specFile.getAbsolutePath();

                    if (parserHelper.error.errorFound) {
                        logger.warning("[PARSING] ERROR: Skipping this file due to a parsing error");
                        continue;
                    } else {
                        logger.info("[PARSING] No errors found while parsing file, continuing on to converting");
                    }
                    // END OF PARSING MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    switch (runVal) {
                        case "stats" : {
                            logger.info("[ACTION: STATS] ");

                            if (batch_id == null) {
                                batch_id = helper.getLargestInt("batch_id") + 1;
                            }

                            CollectStatistics cs = new CollectStatistics(timing, helper, m,
                                    FileHelper.sha256(specFile));
                            cs.setAlteration("none", 0L);
                            cs.getPreviousResults();
                            cs.setMetaData(batch_id,
                                    cmd.getOptionValue(CreePolicyAnalyzerOptionString.OWNER.toString(), null));
                            cs.collectAllStatistics();

                            String query = psm.getSQLInsert(cs.ps);
                            /*DEBUG*/
                            logger.fine("SQL: " + query);

                            Statement stmt = helper.conn.createStatement();
                            boolean error = false;
                            try {
                                int rs = stmt.executeUpdate(query);
                                if (rs != 1) {
                                    error = true;
                                }
                            } catch (SQLException e) {
                                StringWriter errors = new StringWriter();
                                e.printStackTrace(new PrintWriter(errors));
                                logger.severe(errors.toString());
                                logger.severe(e.getMessage());
                                error = true;
                            } finally {
                                stmt.close();
                            }
                            if (error) {
                                logger.severe("Unable to save the following result "
                                        + "(execute this SQL line in the db to save manually):\n" + query);
                            }

                        }
                            break;
                        case "slicing" : {
                            logger.info("[ACTION: SLICING] ");

                            if (batch_id == null) {
                                batch_id = helper.getLargestInt("batch_id") + 1;
                            }

                            String fileHash = FileHelper.sha256(specFile, null);
                            String owner = cmd.getOptionValue(CreePolicyAnalyzerOptionString.OWNER.toString(), null);
                            StaticSlicer slicer = new StaticSlicer(m, m.policyFilename, null, "");

                            Long forwardDuration = System.currentTimeMillis();
                            MohawkT forwardOnly = slicer.forwardPruning(m);
                            forwardOnly.policyFilename = m.policyFilename;
                            forwardDuration = System.currentTimeMillis() - forwardDuration;

                            Long backwardDuration = System.currentTimeMillis();
                            MohawkT backwardOnly = slicer.backwardPruning(m);
                            backwardOnly.policyFilename = m.policyFilename;
                            backwardDuration = System.currentTimeMillis() - backwardDuration;

                            Long bothDuration = System.currentTimeMillis();
                            MohawkT both = slicer.backwardPruning(forwardOnly);
                            both.policyFilename = m.policyFilename;
                            bothDuration = System.currentTimeMillis() - bothDuration;

                            Object[][] slicedPolicies = {{"forwardOnly", forwardOnly, forwardDuration},
                                    {"backwardOnly", backwardOnly, backwardDuration}, {"both", both, bothDuration}};

                            for (int j = 0; j < slicedPolicies.length; j++) {
                                CollectStatistics cs = new CollectStatistics(timing, helper,
                                        (MohawkT) slicedPolicies[j][1], fileHash);
                                cs.setAlteration((String) slicedPolicies[j][0], (Long) slicedPolicies[j][2]);
                                cs.getPreviousResults();
                                cs.setMetaData(batch_id, owner);
                                cs.collectAllStatistics();

                                String query = psm.getSQLInsert(cs.ps);
                                // /*DEBUG*/ System.out.println("SQL: " + query);

                                Statement stmt = helper.conn.createStatement();
                                boolean error = false;
                                try {
                                    int rs = stmt.executeUpdate(query);
                                    if (rs != 1) {
                                        error = true;
                                    }
                                } catch (SQLException e) {
                                    StringWriter errors = new StringWriter();
                                    e.printStackTrace(new PrintWriter(errors));
                                    logger.severe(errors.toString());
                                    logger.severe(e.getMessage());
                                    error = true;
                                } finally {
                                    stmt.close();
                                }
                                if (error) {
                                    logger.severe("Unable to save the following result "
                                            + "(execute this SQL line in the db to save manually):\n" + query);
                                }
                            }

                        }
                            break;
                        case "absref" : {
                            logger.info("[ACTION: Abstraction Refinement] ");

                            if (batch_id == null) {
                                batch_id = helper.getLargestInt("batch_id") + 1;
                            }
                            String fileHash = FileHelper.sha256(specFile, null);
                            String owner = cmd.getOptionValue(CreePolicyAnalyzerOptionString.OWNER.toString(), null);
                            CollectStatistics path = new CollectStatistics(timing, helper, m, fileHash);
                            path.setAlteration("none", 0L);
                            path.getPreviousResults();
                            MohawkT pathPolicy = null;
                            if (path.ps.result_expected.equals("REACHABLE")) {
                                pathPolicy = CollectStatistics.pathToCounterExample(path.ps.path_raw);
                            }

                            AbstractionRefinement refiner = new AbstractionRefinement(m);

                            while (true) {
                                Long refinementDuration = System.currentTimeMillis();
                                String errStr = refiner.calculateNextPolicy();
                                refinementDuration = System.currentTimeMillis() - refinementDuration;

                                if (errStr.isEmpty()) {
                                    CollectStatistics cs = new CollectStatistics(timing, helper, refiner.nextPolicy,
                                            fileHash);
                                    cs.setAlteration(refiner.nextPolicy.generatorName, refinementDuration);
                                    cs.getPreviousResults();
                                    cs.setMetaData(batch_id, owner);
                                    cs.collectAllStatistics();

                                    String query = psm.getSQLInsert(cs.ps);
                                    // /*DEBUG*/ System.out.println("SQL: " + query);

                                    Statement stmt = helper.conn.createStatement();
                                    boolean error = false;
                                    try {
                                        int rs = stmt.executeUpdate(query);
                                        if (rs != 1) {
                                            error = true;
                                        }
                                    } catch (SQLException e) {
                                        StringWriter errors = new StringWriter();
                                        e.printStackTrace(new PrintWriter(errors));
                                        logger.severe(errors.toString());
                                        logger.severe(e.getMessage());
                                        error = true;
                                    } finally {
                                        stmt.close();
                                    }
                                    if (error) {
                                        logger.severe("Unable to save the following result "
                                                + "(execute this SQL line in the db to save manually):\n" + query);
                                    }

                                    if (path.ps.result_expected.equals("REACHABLE")) {
                                        boolean goalStateReachable = true;
                                        for (Rule r : pathPolicy.getAllRules()) {
                                            switch (r._type) {
                                                case ASSIGN :
                                                    goalStateReachable = refiner.nextPolicy.canAssign.getRules()
                                                            .contains(r);
                                                    break;
                                                case REVOKE :
                                                    goalStateReachable = refiner.nextPolicy.canRevoke.getRules()
                                                            .contains(r);
                                                    break;
                                                case ENABLE :
                                                    goalStateReachable = refiner.nextPolicy.canEnable.getRules()
                                                            .contains(r);
                                                    break;
                                                case DISABLE :
                                                    goalStateReachable = refiner.nextPolicy.canDisable.getRules()
                                                            .contains(r);
                                                    break;
                                            }
                                            if (goalStateReachable == false) {
                                                break;
                                            }
                                        }

                                        if (goalStateReachable) {
                                            break;
                                        }
                                    }
                                } else {
                                    if (!errStr.equals("")) {
                                        logger.severe(
                                                "Error for policy " + specFile.getAbsolutePath() + " -- " + errStr);
                                    }
                                    break;
                                }
                            }

                        }
                            break;
                        case "maxabsref" : {
                            logger.info("[ACTION: Max Abstraction Refinement] ");

                            if (batch_id == null) {
                                batch_id = helper.getLargestInt("batch_id") + 1;
                            }
                            String fileHash = FileHelper.sha256(specFile, null);
                            String owner = cmd.getOptionValue(CreePolicyAnalyzerOptionString.OWNER.toString(), null);
                            CollectStatistics path = new CollectStatistics(timing, helper, m, fileHash);
                            path.setAlteration("none", 0L);
                            path.getPreviousResults();

                            AbstractionRefinement refiner = new AbstractionRefinement(m);

                            while (true) {
                                Long refinementDuration = System.currentTimeMillis();
                                String errStr = refiner.calculateNextPolicy();
                                refinementDuration = System.currentTimeMillis() - refinementDuration;

                                if (errStr.isEmpty()) {
                                    CollectStatistics cs = new CollectStatistics(timing, helper, refiner.nextPolicy,
                                            fileHash);
                                    cs.setAlteration("Max-" + refiner.nextPolicy.generatorName, refinementDuration);
                                    cs.getPreviousResults();
                                    cs.setMetaData(batch_id, owner);
                                    cs.collectAllStatistics();

                                    String query = psm.getSQLInsert(cs.ps);
                                    // /*DEBUG*/ System.out.println("SQL: " + query);

                                    Statement stmt = helper.conn.createStatement();
                                    boolean error = false;
                                    try {
                                        int rs = stmt.executeUpdate(query);
                                        if (rs != 1) {
                                            error = true;
                                        }
                                    } catch (SQLException e) {
                                        StringWriter errors = new StringWriter();
                                        e.printStackTrace(new PrintWriter(errors));
                                        logger.severe(errors.toString());
                                        logger.severe(e.getMessage());
                                        error = true;
                                    } finally {
                                        stmt.close();
                                    }
                                    if (error) {
                                        logger.severe("Unable to save the following result "
                                                + "(execute this SQL line in the db to save manually):\n" + query);
                                    }
                                } else {
                                    if (!errStr.isEmpty() && !errStr.equals("No Refinements Found\n")) {
                                        logger.severe(
                                                "Error for policy " + specFile.getAbsolutePath() + " -- " + errStr);
                                    }
                                    break;
                                }
                            }

                        }
                            break;
                        default :
                            logger.severe("The Run Option '" + runVal + "' has not been implemented. "
                                    + "Please see use '-help' option to see which Run Options have been implemented");
                            /* TIMING */timing.cancelTimer(timerName);
                            continue;
                    }
                    // END OF RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    /* TIMING */timing.stopTimer(timerName);
                }
                // END OF INDIVIDUAL FILE LOOP
                ////////////////////////////////////////////////////////////////////////////////
            }

            logger.info("[EOF] MohawkTSolver is done running");

            logger.info("[TIMING] " + timing);
            timing.writeOut(new File(timingFile), false);
        } catch (ParseException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -2;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -1;
        } finally {
            ////////////////////////////////////////////////////////////////////////////////
            // CLEAN UP FILE HANDLES
            for (Handler h : logger.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
            helper.close();
            // CLEAN UP FILE HANDLES
            ////////////////////////////////////////////////////////////////////////////////
        }
        return 0;
    }

    public CommandLine init(String[] args) throws ParseException, SecurityException, IOException, Exception {
        Options options = new Options();
        setupOptions(options);
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse(options, args);

        setupLoggerOptions(cmd, options);
        if (setupReturnImmediatelyOptions(cmd, options)) { return null; }

        setupUserPreferenceOptions(cmd, options);
        setupFileOptions(cmd, options);
        setupResultOptions(cmd, options);
        setupControlOptions(cmd, options);

        return cmd;
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "mohawk",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    public void printHelp(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(CreePolicyAnalyzerOptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (NumberFormatException e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new NumberFormatException("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    /** Adds all of the available options to input parameter.
     * 
     * @param options */
    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(CreePolicyAnalyzerOptionString.HELP.toString(), false, "Print this message");
        options.addOption(CreePolicyAnalyzerOptionString.VERSION.toString(), false,
                "Prints the version (" + VERSION + ") information");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create(CreePolicyAnalyzerOptionString.LOGLEVEL.toString()));

        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create(CreePolicyAnalyzerOptionString.LOGFILE.toString()));

        options.addOption(CreePolicyAnalyzerOptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        options.addOption(
                OptionBuilder.withArgName("dbfile").withDescription("The file where the result should be stored")
                        .hasArg().create(CreePolicyAnalyzerOptionString.DBFILE.toString()));

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(CreePolicyAnalyzerOptionString.MAXW.toString()));

        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(CreePolicyAnalyzerOptionString.LINESTR.toString()));

        options.addOption(CreePolicyAnalyzerOptionString.DEBUG.toString(), false,
                "Add this flag to get more ouput about the steps that are taken by Mohawk+T "
                        + "(drastically slows down performance)");

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the RBAC Spec file or Folder if the 'bulk' option is set").hasArg()
                .create(CreePolicyAnalyzerOptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + fileHelper.fileExt + "'")
                .hasArg().create(CreePolicyAnalyzerOptionString.SPECEXT.toString()));

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Functional Options
        options.addOption(CreePolicyAnalyzerOptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Control Flags
        options.addOption(
                OptionBuilder.withArgName("name").withDescription("The owner of the policy, or folder of policy files")
                        .hasArg().create(CreePolicyAnalyzerOptionString.OWNER.toString()));

        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Actionable Options
        options.addOption(OptionBuilder.withArgName("option")
                .withDescription("Available options: stats, slicing, absref, maxabsref.").hasArg()
                .create(CreePolicyAnalyzerOptionString.RUN.toString()));
        ///////////////////////////////////////////////////////////////////////////////////////////
    }

    /* ********* FUNCTIONS ********* */
    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    /** Sets up the options that do not process anything in detail, but returns a specific informative result.<br/>
     * Examples include:
     * <ul>
     * <li>Help
     * <li>Version
     * <li>Check NuSMV programs
     * </ul>
     * 
     * @param cmd
     * @param options
     * @return
     * @throws NumberFormatException */
    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(CreePolicyAnalyzerOptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        return false;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.FINEST);// Default Level
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(CreePolicyAnalyzerOptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.NOHEADER.toString())) {
            WriteCSVFileHeader = false;
        }

        // Set File Logger
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(CreePolicyAnalyzerOptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue(CreePolicyAnalyzerOptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(cmd.getOptionValue(CreePolicyAnalyzerOptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }

        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {

            File logfile = new File(Logger_filepath);

            if (!logfile.exists()) {
                logfile.getParentFile().mkdirs();

                logfile.createNewFile();
                if (WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                    writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }
            }

            fileHandler = new FileHandler(Logger_filepath, true);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    /** Looks for the following options and sets up the program accordingly:
     * <ul>
     * <li>{@link CreePolicyAnalyzerOptionString#ABSTRACTION_REFINEMENT}
     * <li>{@link CreePolicyAnalyzerOptionString#BOUND_ESTIMATION}
     * <li>{@link CreePolicyAnalyzerOptionString#DEBUG}
     * <li>{@link CreePolicyAnalyzerOptionString#NO_SLICING}
     * <li>{@link CreePolicyAnalyzerOptionString#PROGRAM}
     * <li>{@link CreePolicyAnalyzerOptionString#SKIP_RUNNING}
     * </ul>
     * 
     * @param cmd
     * @param options */
    private void setupControlOptions(CommandLine cmd, Options options) {
        /////////////////////////////////////////////////////////////////////////
        // DEBUG FLAG
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.DEBUG.toString())) {
            debug = true;
        } else {
            debug = false;
        }
        logger.info("[Option] Debug Mode: " + ((debug) ? "ENABLED" : "DISABLED"));
        /////////////////////////////////////////////////////////////////////////
    }

    private void setupResultOptions(CommandLine cmd, Options options) throws IOException {
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.DBFILE.toString())) {
            dbFile = cmd.getOptionValue(CreePolicyAnalyzerOptionString.DBFILE.toString());
        }
        logger.info("[OPTION] Database File: " + dbFile);
    }

    private void setupFileOptions(CommandLine cmd, Options options) {
        // Load in SPEC Files
        // SMV File
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.SMVFILE.toString())) {
            if (cmd.getOptionValue(CreePolicyAnalyzerOptionString.SMVFILE.toString()).equals("n")) {
                logger.fine("[OPTION] Using temporary SMV Files - will be deleted after each use");
                fileHelper.smvDeleteFile = true;
            } else {
                logger.fine("[OPTION] Using a specific SMV File: "
                        + cmd.getOptionValue(CreePolicyAnalyzerOptionString.SMVFILE.toString()));
                fileHelper.smvFilepath = cmd.getOptionValue(CreePolicyAnalyzerOptionString.SMVFILE.toString());
            }
        } else {
            logger.fine("[OPTION] No SMV Filename included, saving file under: " + fileHelper.smvFilepath);
        }

        // Grab the SPEC file
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.SPECFILE.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File: "
                    + cmd.getOptionValue(CreePolicyAnalyzerOptionString.SPECFILE.toString()));
            fileHelper.specFile = cmd.getOptionValue(CreePolicyAnalyzerOptionString.SPECFILE.toString());
        } else {
            logger.fine("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(CreePolicyAnalyzerOptionString.SPECEXT.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(CreePolicyAnalyzerOptionString.SPECEXT.toString()));
            fileHelper.fileExt = cmd.getOptionValue(CreePolicyAnalyzerOptionString.SPECEXT.toString());
        } else {
            logger.fine("[OPTION] Using the default SPEC File Extension: " + fileHelper.fileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.BULK.toString())) {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Enabled");
            fileHelper.bulk = true;
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
            fileHelper.bulk = false;
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(CreePolicyAnalyzerOptionString.MAXW.toString());
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(CreePolicyAnalyzerOptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(CreePolicyAnalyzerOptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }
}
