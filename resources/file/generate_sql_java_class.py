'''
This python file takes in a CSV file and produces 2 SQL files and a Java Class.
<br/>
The input CSV file has the following format:
<pre>
COLUMN_NAME, SQL_DATA_TYPE, COLUMN_DESCRIPTION, OTHER_SQL_COMMANDS
...
...
SMALL_SEPARATOR
...
</pre>
The separators are used to denote when a comment (in either Java or SQL) should be inserted, 
to make the code more readable.
<br/>
The output files are:
<ul>
<li>PolicyStats-column-description-table.sql -- Drops any previous table and 
then creates the table to store the column descriptions. 
Then it fills in each column description for the PolicyStats table.</li>
<li>PolicyStats-table.sql -- Doesn't drop any table, errors out if a table already exists. 
Creates the table PolicyStats to store the each policies statistics.</li>
<li>PolicyStatistics.java -- </li>
</ul>
'''
import sys
import csv
import os

java_policystatistics_filename = 'PolicyStatistics.java'
java_policystatisticsblock_filename = 'PolicyStatisticsBlock.java'
sql_policystats_filename = 'PolicyStats-table.sql'
sql_policystats_descriptions_filename = 'PolicyStats-column-description-table.sql'
sql_policystats_schema_filename = 'PolicyStats_Schema.txt'
text_policystats_columns_filename = 'PolicyStats_Columns.txt'

blocks = ['ca', 'cr', 'ce', 'cd']
policyText = {
    'ca': 'the CanAssign Block',
    'cr': 'the CanRevoke Block',
    'ce': 'the CanEnable Block',
    'cd': 'the CanDisable Block'
}

files = [
    # populated in function so variables are loaded
]


def load_files():
    global files
    files = [
        {
            'filename': java_policystatistics_filename,
            'output': 'java',
            'prefix': java_policystatistics_prefix,
            'suffix': java_policystatistics_suffix,
            'sep': java_separator,
            'small_sep': java_small_separator,
            'skip_first':False
        }, {
            'filename': sql_policystats_filename,
            'output': 'sql_table',
            'prefix': sql_table_prefix,
            'suffix': sql_table_suffix,
            'sep': sql_separator,
            'small_sep': sql_small_separator,
            'skip_first':False
        }, {
            'filename': sql_policystats_descriptions_filename,
            'output': 'sql_insert',
            'prefix': sql_description_prefix,
            'suffix': sql_description_suffix,
            'sep': sql_separator.replace('\t', '') + sql_insert_str + "('','','','================================================');\n",
            'small_sep': sql_small_separator.replace('\t', ''),
            'skip_first':True
        }, {
            'filename': sql_policystats_schema_filename,
            'output': 'schema',
            'prefix': '',
            'suffix': '',
            'sep': '',
            'small_sep': '',
            'skip_first':False
        }, {
            'filename': text_policystats_columns_filename,
            'output': 'text_column',
            'prefix': '',
            'suffix': '',
            'sep': '',
            'small_sep': '',
            'skip_first':False
        },
    ]


# Holds all other columns and puts these at the top
top = []
# holds the columns which start with 'result_'
result = []
# holds the columns which start with 'path_'
path = []
# holds the columns which start with 'stat_' but not 'stat_all_'
stat_misc = []
# holds the columns which start with 'stat_all_' (repeats for CA/CR/CE/CD)
stat_all = []
# holds the columns which start with 'num_' but not 'num_all_'
num_misc = []
# holds the columns which start with 'num_all_' (repeats for CA/CR/CE/CD)
num_all = []


def row_placement(id, last_id):
    if id == 'SMALL_SEPARATOR':
        id = last_id

    if id.startswith('stat_all_'):
        return stat_all
    elif id.startswith('stat_'):
        return stat_misc
    elif id.startswith('num_all'):
        return num_all
    elif id.startswith('num_'):
        return num_misc
    elif id.startswith('result_'):
        return result
    elif id.startswith('path_'):
        return path
    else:
        return top


def main(csvfile):
    '''
    Read in the data from the csvfile and store in the variables:
        * top, result, stat_misc, stat_all, num_misc, num_all
    Where the data in *_all will be duplicated for each block section (CA/CR/CE/CD)
    Write out the following files:
        * java_filename
        * sql_policystats_filename
        * sql_policystats_descriptions_filename
        * sql_policystats_schema_filename
    '''
    with open(csvfile, 'r') as f:
        print('Reading in data from {}'.format(csvfile))
        reader = csv.reader(f)
        last_id = None
        for row in reader:
            # row: [SMALL_SEPARATOR], or
            # row: [COLUMN_ID, DATA_TYPE, DESCRIPTION, OTHER_OPTIONS], or
            # row: ['', '', '', '']
            # print row

            if not row[0]:
                # skipping blank row
                continue

            b = row_placement(row[0], last_id)
            b.append(row)

            if row[0] != 'SMALL_SEPARATOR':
                last_id = row[0]

    load_files()
    for f in files:
        write_file(f['filename'], f['output'], f['prefix'],
                   f['suffix'], f['sep'], f['small_sep'], f['skip_first'])
    # write_sql_policystats()
    # write_java_policystats()
    write_java_policystatsblock()
    # write_sql_policystats_description()
    # write_sql_schema()


def write_small_sep(output, f):
    if output == 'schema':
        pass
    elif output == 'text_column':
        pass
    elif output == 'sql_table':
        f.write(sql_small_separator)
    elif output == 'sql_insert':
        f.write(sql_small_separator.replace('\t', ''))
        f.write(sql_insert_str + "('','','','==========');\n")
        f.write(sql_small_separator.replace('\t', ''))
    elif output == 'java':
        f.write(java_small_separator)

def update_line(line, block, replace):
    if replace != None:
        line = line.replace(replace[0], replace[1])
    if block != None:
        line = line.replace('_all_', '_'+block+'_')
        line = line.replace('the policy', policyText[block])
    return line

def write_row(output, f, lines, block=None, last=False, suffix=None, replace=None, i=0):
    '''
    output = {schema, sql_table, sql_insert, java, text_column}
    '''
    j = 0
    for l in lines:
        if l[0] != 'SMALL_SEPARATOR':
            if output == 'schema':
                not_null = '0'
                if 'NOT NULL' in l[3]:
                    not_null = '1'
                line = "{},{},{},{}\n".format(i, l[0], l[1], not_null)
                line = update_line(line, block, replace)
                f.write(line)
            elif output == 'text_column':
                line = update_line(l[0] + ',\n', block, replace)
                f.write(line)
            elif output == 'sql_table':
                line = '\t-- {}\n\t{}\t{} {},\n'.format(l[2], l[0], l[1], l[3])
                line = update_line(line, block, replace)
                # Remove the last comma to make the create table statement correct
                if last == True and j == len(lines)-1:
                    line = line.replace(',\n', '\n')
                f.write(line)
            elif output == 'sql_insert':
                line = "('{}', '{}', '{}', '{}');\n".format(
                    i,l[0], l[1], l[2].replace("'", '"'))
                line = update_line(line, block, replace)
                f.write(sql_insert_str + line)
            elif output == 'java':
                java_type = get_java_type(l[1])
                if suffix == None:
                    line = '\t// {}\n\tpublic {} {};\n'.format(
                        l[2], java_type, l[0])
                else:
                    line = '\t// {}\n\tpublic {} {}{};\n'.format(
                        l[2], java_type, l[0], suffix)
                line = update_line(line, block, replace)
                f.write(line)
            # Count number of actual line and not small separators
            i += 1
        else:
            write_small_sep(output, f)
        j += 1 # count every line
    return i

def write_file(filename, output, prefix, suffix, sep, small_sep, skip_first):
    with open(filename, 'w') as f:
        print('Writing to {}'.format(filename))
        f.write(prefix)
        if not skip_first:
            f.write(sep)
        i = 0
        # TOP
        i = write_row(output, f, top, i=i)
        f.write(sep)
        # RESULT
        i = write_row(output, f, result, i=i)
        f.write(sep)
        # STAT MISC
        i = write_row(output, f, stat_misc, i=i)
        f.write(small_sep)
        # STAT ALL + BLOCKS
        i = write_row(output, f, stat_all, i=i)
        for block in blocks:
            f.write(small_sep)
            i = write_row(output, f, stat_all, block=block, i=i)
        # NUM MISC
        f.write(sep)
        i = write_row(output, f, num_misc, i=i)
        f.write(small_sep)
        # NUM ALL + BLOCKS
        i = write_row(output, f, num_all, i=i)
        for block in blocks:
            f.write(sep)
            i = write_row(output, f, num_all, block=block, i=i)

        # PATH MISC
        f.write(sep)
        i = write_row(output, f, path, i=i)
        f.write(small_sep)
        # PATH ALL + BLOCKS
        i = write_row(output, f, num_all, i=i, replace=('num_all_', 'path_all_'))
        j = 0
        for block in blocks:
            f.write(sep)
            if j == len(blocks) - 1:
                last = True
            else:
                last = False
            i = write_row(output, f, num_all, block=block, i=i, last=last, replace=('num_all_', 'path_all_'))
            j += 1

        f.write(sep)
        f.write(suffix)

def get_java_type(sql_type):
    sql_type = sql_type.upper().strip()
    if sql_type.startswith('VARCHAR'):
        return 'String'
    elif sql_type == 'TEXT':
        return 'String'
    elif sql_type == 'INTEGER':
        return 'Integer'
    elif sql_type == 'REAL':
        return 'Double'
    elif sql_type == 'DATETIME':
        return 'Timestamp'
    elif sql_type == 'CHAR':
        return 'Character'

    return 'UNKNOWN_DATATYPE_{}_'.format(sql_type)

def write_java_policystatsblock():
    with open(java_policystatisticsblock_filename, 'w') as f:
        print('Writing to {}'.format(java_policystatisticsblock_filename))
        f.write(java_policystatisticsblock_prefix)
        f.write(java_separator_half)
        # STAT ALL + BLOCKS
        write_row('java', f, stat_all)
        f.write(java_separator)
        # NUM ALL + BLOCKS
        write_row('java', f, num_all, suffix=' = 0')
        f.write(java_separator_half)
        f.write(java_policystatisticsblock_suffix)

#######################################################################
######################TEMPLATE STRINGS#################################
#######################################################################
sql_separator = '\t' + ('-' * 80) + '\n\n\t' + ('-' * 80) + '\n'
sql_small_separator = '\t' + '--' + ('=' * 35) + '\n'

java_separator_half = '\t' + ('/' * 80) + '\n'
java_separator = java_separator_half + '\n' + java_separator_half
java_small_separator = '\t' + '//' + ('=' * 35) + '\n'

java_policystatistics_prefix = r'''// This file was automatically created by `generate_sql_java_class.py`
package cree.analyzer.results;

import java.lang.reflect.Field;
import java.sql.Timestamp;

/**
 * This class will store the Statistical results for a single policy file, 
 * it will then (in most cases) be written into a results file (most likely SQLite3). 
 * @author Jonathan Shahen
 *
 */
public class PolicyStatistics {
    ////////////////////////////////////////////////////////////////////////////////
    public PolicyStatistics(String policySHA256) {
        if (policySHA256 == null) { throw new IllegalArgumentException("policySHA256 cannot be NULL"); }
        file_sha256 = policySHA256;
    }
'''

java_policystatistics_suffix = r'''
    ////////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @param psb The PolicyStatisticsBlock object that is full of values
     * @param prefix Can be either: 'num', 'path'
     * @param key The block, can be either: 'all', 'ca', 'cr', 'ce', 'cd' 
     */
    public void importBlock(PolicyStatisticsBlock psb, String prefix, String key) {
        Class<? extends PolicyStatistics> c = this.getClass();
        Field ps_f;
        for (Field psb_f : psb.getClass().getDeclaredFields()) {
            try {
                String name = psb_f.getName();

                name = name.replace("num_", prefix + "_");
                if (!key.equals("all")) {
                    name = name.replace(prefix + "_all_", prefix + "_" + key + "_");
                    if (prefix.equals("num")) {
                        name = name.replace("stat_all_", "stat_" + key + "_");
                    }
                }
                ps_f = c.getDeclaredField(name);
                if (psb_f.get(psb) != null) {
                    ps_f.set(this, psb_f.get(psb));
                }
            } catch (Exception e) {}
        }
    }
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PolicyStatistics {");

        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.get(this) != null) {
                    sb.append(field.getName()).append("=").append(field.get(this)).append(", ");
                }
            } catch (Exception e) {}
        }
        sb.append("}");
        return sb.toString();
    }
}
'''

java_policystatisticsblock_prefix = r'''// This file was automatically created by `generate_sql_java_class.py`
package cree.analyzer.results;

import java.lang.reflect.Field;

public class PolicyStatisticsBlock {
'''

java_policystatisticsblock_suffix = r'''
    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PolicyStatisticsBlock {");

        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.get(this) != null) {
                    sb.append(field.getName()).append("=").append(field.get(this)).append(", ");
                }
            } catch (Exception e) {}
        }
        sb.append("}");
        return sb.toString();
    }
}
'''
sql_table_prefix = 'CREATE TABLE PolicyStats\n(\n'
sql_table_suffix = ')'
sql_insert_str = 'INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES'
sql_description_prefix = r'''-- This file was automatically created by `generate_sql_java_class.py`
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: PolicyStats_Description
DROP TABLE IF EXISTS PolicyStats_Description;

CREATE TABLE PolicyStats_Description
(
    offset INTEGER,
    [column] VARCHAR (80) NOT NULL,
    data_type VARCHAR (50) NOT NULL,
    [description] TEXT NOT NULL
);
'''

sql_description_suffix = r'''
COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
'''


if __name__ == '__main__':
    if(len(sys.argv) > 2):
        print('Invalid command line inputs')
        print('{} csvfile'.format(sys.argv[0]))
        sys.exit(0)
    if len(sys.argv) == 1:
        # os.chdir(os.path.dirname(sys.argv[0]))
        main('PolicyStats-table-base.csv')
    else:
        main(sys.argv[1])
    sys.exit(0)
