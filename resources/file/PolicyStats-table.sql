CREATE TABLE PolicyStats
(
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- Unique row id
	id	INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	-- When the policy analyzer is performing a BULK analysis on a folder of policy files, each new row will have the same batch_id.
	batch_id	INTEGER NOT NULL,
	-- The date that this row is created (set automatically upon inserting into the table)
	date_created	DATETIME NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
	-- This field is initialized to date_created, but if the policy editor updates unset values, it will update this field to indicate that the row has been updated.
	last_updated	DATETIME NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
	-- The name of the program which entered this row into the database (can be retroactively updated).
	created_by	VARCHAR(80) NOT NULL,
	-- The policy file's name (does not include the file path). For in-memory policies a special name can be used to indicate it.
	filename	VARCHAR(256) ,
	-- The policy file's parent folder name (does not include the file path). Useful for grouping similar policies. For in-memeory policies a special name can be used to indicate it.
	folder	VARCHAR(256) ,
	-- The complete folder path to the policy file. Can be NULL if the policy is in-memory.
	filepath	TEXT ,
	-- The SHA256 of the file. In-memory policies can get the text version and SHA256 that version.
	file_sha256	VARCHAR(257) ,
	-- The creator of the policy file (which testsuite did it come from)
	owner	VARCHAR(256) ,
	-- This field indicates the theoretical complexity of the solver required to solve this policy file
	complexity	VARCHAR(50) ,
	-- This field is either NULL, or some text that describes how it is modified from the original file
	alteration_method	TEXT ,
	-- This field holds the duration, in milliseconds, that the alteration method took. It is NULL if the alteration method is NULL
	alteration_duration	INTEGER ,
	-- The result returned from running a polynomial Heuristic algorithm. NULL (not present)
	heuristics_result	TEXT ,
	-- The time, in milliseconds, that it took to run the heuristic function on the policy.
	heuristics_duration	INTEGER ,
	-- If the heuristic function returned 'R' or 'U', then this field contains the reason for how it knows that result
	heuristics_description	TEXT ,
	-- Uses Cree's Bound Estimation Algorithm
	bound_estimation	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- If the policy file comes with a 'Expected' tag, then the result is stored here. NULL (not present)
	result_expected	TEXT ,
	-- The result returned by the program Cree. NULL (has not been run)
	result_cree	TEXT ,
	-- The duration in milliseconds of when this policy was run through the Cree program.
	result_cree_dur	INTEGER ,
	-- The result returned by the program Mohawk+T. NULL (has not been run)
	result_mohawk_t	TEXT ,
	-- The duration in milliseconds of when this policy was run through the Mohawk+T program.
	result_mohawk_t_dur	INTEGER ,
	-- The result returned by the program ASASPTime NSA. NULL (has not been run)
	result_asasptime_nsa	TEXT ,
	-- The duration in milliseconds of when this policy was run through the ASASPTime NSA program.
	result_asasptime_nsa_dur	INTEGER ,
	-- The result returned by the program ASASPTime SA. NULL (has not been run)
	result_asasptime_sa	TEXT ,
	-- The duration in milliseconds of when this policy was run through the ASASPTime SA program.
	result_asasptime_sa_dur	INTEGER ,
	-- The result returned by the program TREDRole. NULL (has not been run)
	result_uzun_tredrole	TEXT ,
	-- The duration in milliseconds of when this policy was run through the TREDRole program.
	result_uzun_tredrole_dur	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The theoretical max size required to store the current state of the ATRBAC policy, as described by the policy
	stat_size_max	INTEGER ,
	-- The theoretical min size required to store the current state of the ATRBAC policy, as described by the policy and by incorporating the bound estimation tightening techniques from the Cree journal paper (Shahen et.al.)
	stat_size_min	INTEGER ,
	-- The percentage of rules that are Can Assign rules compared to the total number of rules [0.0, 100.0]
	stat_ca_rules_percent	REAL ,
	-- The percentage of rules that are Can Revoke rules compared to the total number of rules [0.0, 100.0]
	stat_cr_rules_percent	REAL ,
	-- The percentage of rules that are Can Enable rules compared to the total number of rules [0.0, 100.0]
	stat_ce_rules_percent	REAL ,
	-- The percentage of rules that are Can Disable rules compared to the total number of rules [0.0, 100.0]
	stat_cd_rules_percent	REAL ,
	--===================================
	-- The average length of precondition for all rules in the policy.
	stat_all_mean_cond	REAL ,
	-- The standard deviation (population) for the length of precondition for all rules in the policy.
	stat_all_stddevp_cond	REAL ,
	-- The average length of time-slot array for all rules in the policy.
	stat_all_mean_tsarray	REAL ,
	-- The standard deviation (population) for the length of time-slot array for all rules in the policy.
	stat_all_stddevp_tsarray	REAL ,
	--===================================
	-- The average length of precondition for all rules in the CanAssign Block.
	stat_ca_mean_cond	REAL ,
	-- The standard deviation (population) for the length of precondition for all rules in the CanAssign Block.
	stat_ca_stddevp_cond	REAL ,
	-- The average length of time-slot array for all rules in the CanAssign Block.
	stat_ca_mean_tsarray	REAL ,
	-- The standard deviation (population) for the length of time-slot array for all rules in the CanAssign Block.
	stat_ca_stddevp_tsarray	REAL ,
	--===================================
	-- The average length of precondition for all rules in the CanRevoke Block.
	stat_cr_mean_cond	REAL ,
	-- The standard deviation (population) for the length of precondition for all rules in the CanRevoke Block.
	stat_cr_stddevp_cond	REAL ,
	-- The average length of time-slot array for all rules in the CanRevoke Block.
	stat_cr_mean_tsarray	REAL ,
	-- The standard deviation (population) for the length of time-slot array for all rules in the CanRevoke Block.
	stat_cr_stddevp_tsarray	REAL ,
	--===================================
	-- The average length of precondition for all rules in the CanEnable Block.
	stat_ce_mean_cond	REAL ,
	-- The standard deviation (population) for the length of precondition for all rules in the CanEnable Block.
	stat_ce_stddevp_cond	REAL ,
	-- The average length of time-slot array for all rules in the CanEnable Block.
	stat_ce_mean_tsarray	REAL ,
	-- The standard deviation (population) for the length of time-slot array for all rules in the CanEnable Block.
	stat_ce_stddevp_tsarray	REAL ,
	--===================================
	-- The average length of precondition for all rules in the CanDisable Block.
	stat_cd_mean_cond	REAL ,
	-- The standard deviation (population) for the length of precondition for all rules in the CanDisable Block.
	stat_cd_stddevp_cond	REAL ,
	-- The average length of time-slot array for all rules in the CanDisable Block.
	stat_cd_mean_tsarray	REAL ,
	-- The standard deviation (population) for the length of time-slot array for all rules in the CanDisable Block.
	stat_cd_stddevp_tsarray	REAL ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of goal roles in the policy
	num_goal_roles	INTEGER ,
	--===================================
	-- The number of all rules in the policy
	num_all_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	num_all_timeslots	INTEGER ,
	-- The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)
	num_all_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the policy
	num_all_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the policy
	num_all_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the policy
	num_all_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the policy
	num_all_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the policy
	num_all_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	num_all_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the policy
	num_all_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the policy
	num_all_rules_admin_true	INTEGER ,
	-- The number of rules in the policy where the pre-condition is set to TRUE.
	num_all_rules_precond_true	INTEGER ,
	-- The number of rules in the policy where the pre-condition is only positive roles.
	num_all_rules_precond_pos	INTEGER ,
	-- The number of rules in the policy where the pre-condition is only negative roles.
	num_all_rules_precond_neg	INTEGER ,
	-- The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
	num_all_rules_precond_mixed	INTEGER ,
	-- The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
	num_all_rules_precond_goal	INTEGER ,
	-- The number of rules in the policy where the target role is also one of the goal roles
	num_all_rules_target_goal	INTEGER ,
	-- The number of rules in the policy where the admin condition is one of the goal roles.
	num_all_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the policy that are startable, where a rule's precondition is TRUE or all negative roles.
	num_all_rules_startable	INTEGER ,
	-- The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.
	num_all_rules_truely_startable	INTEGER ,
	-- The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."
	num_all_rules_invokable	INTEGER ,
	-- The number of rules in the policy that contain a positive precondition that is unassignable.
	num_all_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the policy
	num_all_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the policy
	num_all_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
	num_all_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)
	num_all_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the policy that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
	num_all_longest_rules_cond	INTEGER ,
	-- The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'num_all_roles_longest_pos_cond' 
	num_all_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to 'num_all_timeslots_longest_tsarray'
	num_all_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanAssign Block
	num_ca_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	num_ca_timeslots	INTEGER ,
	-- The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)
	num_ca_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanAssign Block
	num_ca_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanAssign Block
	num_ca_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanAssign Block
	num_ca_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanAssign Block
	num_ca_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanAssign Block
	num_ca_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	num_ca_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block
	num_ca_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanAssign Block
	num_ca_rules_admin_true	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is set to TRUE.
	num_ca_rules_precond_true	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is only positive roles.
	num_ca_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is only negative roles.
	num_ca_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.
	num_ca_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles
	num_ca_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanAssign Block where the target role is also one of the goal roles
	num_ca_rules_target_goal	INTEGER ,
	-- The number of rules in the CanAssign Block where the admin condition is one of the goal roles.
	num_ca_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanAssign Block that are startable, where a rule's precondition is TRUE or all negative roles.
	num_ca_rules_startable	INTEGER ,
	-- The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	num_ca_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	num_ca_rules_invokable	INTEGER ,
	-- The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.
	num_ca_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanAssign Block
	num_ca_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanAssign Block
	num_ca_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)
	num_ca_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)
	num_ca_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanAssign Block that contain a pre-condition of length equal to 'num_ca_roles_longest_cond' 
	num_ca_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to 'num_ca_roles_longest_pos_cond' 
	num_ca_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to 'num_ca_timeslots_longest_tsarray'
	num_ca_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanRevoke Block
	num_cr_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	num_cr_timeslots	INTEGER ,
	-- The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)
	num_cr_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanRevoke Block
	num_cr_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanRevoke Block
	num_cr_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanRevoke Block
	num_cr_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanRevoke Block
	num_cr_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanRevoke Block
	num_cr_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	num_cr_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block
	num_cr_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanRevoke Block
	num_cr_rules_admin_true	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.
	num_cr_rules_precond_true	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is only positive roles.
	num_cr_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is only negative roles.
	num_cr_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.
	num_cr_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles
	num_cr_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanRevoke Block where the target role is also one of the goal roles
	num_cr_rules_target_goal	INTEGER ,
	-- The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.
	num_cr_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanRevoke Block that are startable, where a rule's precondition is TRUE or all negative roles.
	num_cr_rules_startable	INTEGER ,
	-- The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	num_cr_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	num_cr_rules_invokable	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.
	num_cr_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block
	num_cr_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanRevoke Block
	num_cr_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)
	num_cr_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)
	num_cr_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanRevoke Block that contain a pre-condition of length equal to 'num_cr_roles_longest_cond' 
	num_cr_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to 'num_cr_roles_longest_pos_cond' 
	num_cr_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to 'num_cr_timeslots_longest_tsarray'
	num_cr_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanEnable Block
	num_ce_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	num_ce_timeslots	INTEGER ,
	-- The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)
	num_ce_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanEnable Block
	num_ce_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanEnable Block
	num_ce_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanEnable Block
	num_ce_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanEnable Block
	num_ce_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanEnable Block
	num_ce_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	num_ce_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block
	num_ce_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanEnable Block
	num_ce_rules_admin_true	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is set to TRUE.
	num_ce_rules_precond_true	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is only positive roles.
	num_ce_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is only negative roles.
	num_ce_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.
	num_ce_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles
	num_ce_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanEnable Block where the target role is also one of the goal roles
	num_ce_rules_target_goal	INTEGER ,
	-- The number of rules in the CanEnable Block where the admin condition is one of the goal roles.
	num_ce_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanEnable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	num_ce_rules_startable	INTEGER ,
	-- The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	num_ce_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	num_ce_rules_invokable	INTEGER ,
	-- The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.
	num_ce_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanEnable Block
	num_ce_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanEnable Block
	num_ce_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)
	num_ce_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)
	num_ce_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanEnable Block that contain a pre-condition of length equal to 'num_ce_roles_longest_cond' 
	num_ce_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to 'num_ce_roles_longest_pos_cond' 
	num_ce_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to 'num_ce_timeslots_longest_tsarray'
	num_ce_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanDisable Block
	num_cd_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	num_cd_timeslots	INTEGER ,
	-- The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)
	num_cd_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanDisable Block
	num_cd_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanDisable Block
	num_cd_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanDisable Block
	num_cd_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanDisable Block
	num_cd_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanDisable Block
	num_cd_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	num_cd_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block
	num_cd_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanDisable Block
	num_cd_rules_admin_true	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is set to TRUE.
	num_cd_rules_precond_true	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is only positive roles.
	num_cd_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is only negative roles.
	num_cd_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.
	num_cd_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles
	num_cd_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanDisable Block where the target role is also one of the goal roles
	num_cd_rules_target_goal	INTEGER ,
	-- The number of rules in the CanDisable Block where the admin condition is one of the goal roles.
	num_cd_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanDisable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	num_cd_rules_startable	INTEGER ,
	-- The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	num_cd_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	num_cd_rules_invokable	INTEGER ,
	-- The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.
	num_cd_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanDisable Block
	num_cd_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanDisable Block
	num_cd_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)
	num_cd_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)
	num_cd_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanDisable Block that contain a pre-condition of length equal to 'num_cd_roles_longest_cond' 
	num_cd_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to 'num_cd_roles_longest_pos_cond' 
	num_cd_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to 'num_cd_timeslots_longest_tsarray'
	num_cd_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- This field contains the raw text output from Cree; each rule is separated by ' -> ', the rule type is denoted in the comment preceding each rule. The counter example starts with 'Initial State' and end at the 'Goal State'.
	path_raw	TEXT ,
	--===================================
	-- The number of all rules in the policy
	path_all_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	path_all_timeslots	INTEGER ,
	-- The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)
	path_all_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the policy
	path_all_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the policy
	path_all_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the policy
	path_all_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the policy
	path_all_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the policy
	path_all_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	path_all_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the policy
	path_all_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the policy
	path_all_rules_admin_true	INTEGER ,
	-- The number of rules in the policy where the pre-condition is set to TRUE.
	path_all_rules_precond_true	INTEGER ,
	-- The number of rules in the policy where the pre-condition is only positive roles.
	path_all_rules_precond_pos	INTEGER ,
	-- The number of rules in the policy where the pre-condition is only negative roles.
	path_all_rules_precond_neg	INTEGER ,
	-- The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
	path_all_rules_precond_mixed	INTEGER ,
	-- The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
	path_all_rules_precond_goal	INTEGER ,
	-- The number of rules in the policy where the target role is also one of the goal roles
	path_all_rules_target_goal	INTEGER ,
	-- The number of rules in the policy where the admin condition is one of the goal roles.
	path_all_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the policy that are startable, where a rule's precondition is TRUE or all negative roles.
	path_all_rules_startable	INTEGER ,
	-- The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.
	path_all_rules_truely_startable	INTEGER ,
	-- The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."
	path_all_rules_invokable	INTEGER ,
	-- The number of rules in the policy that contain a positive precondition that is unassignable.
	path_all_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the policy
	path_all_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the policy
	path_all_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
	path_all_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)
	path_all_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the policy that contain a pre-condition of length equal to 'path_all_roles_longest_cond' 
	path_all_longest_rules_cond	INTEGER ,
	-- The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'path_all_roles_longest_pos_cond' 
	path_all_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to 'path_all_timeslots_longest_tsarray'
	path_all_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanAssign Block
	path_ca_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	path_ca_timeslots	INTEGER ,
	-- The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)
	path_ca_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanAssign Block
	path_ca_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanAssign Block
	path_ca_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanAssign Block
	path_ca_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanAssign Block
	path_ca_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanAssign Block
	path_ca_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	path_ca_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block
	path_ca_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanAssign Block
	path_ca_rules_admin_true	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is set to TRUE.
	path_ca_rules_precond_true	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is only positive roles.
	path_ca_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is only negative roles.
	path_ca_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.
	path_ca_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles
	path_ca_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanAssign Block where the target role is also one of the goal roles
	path_ca_rules_target_goal	INTEGER ,
	-- The number of rules in the CanAssign Block where the admin condition is one of the goal roles.
	path_ca_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanAssign Block that are startable, where a rule's precondition is TRUE or all negative roles.
	path_ca_rules_startable	INTEGER ,
	-- The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	path_ca_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	path_ca_rules_invokable	INTEGER ,
	-- The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.
	path_ca_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanAssign Block
	path_ca_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanAssign Block
	path_ca_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)
	path_ca_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)
	path_ca_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanAssign Block that contain a pre-condition of length equal to 'path_ca_roles_longest_cond' 
	path_ca_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to 'path_ca_roles_longest_pos_cond' 
	path_ca_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to 'path_ca_timeslots_longest_tsarray'
	path_ca_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanRevoke Block
	path_cr_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	path_cr_timeslots	INTEGER ,
	-- The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)
	path_cr_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanRevoke Block
	path_cr_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanRevoke Block
	path_cr_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanRevoke Block
	path_cr_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanRevoke Block
	path_cr_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanRevoke Block
	path_cr_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	path_cr_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block
	path_cr_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanRevoke Block
	path_cr_rules_admin_true	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.
	path_cr_rules_precond_true	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is only positive roles.
	path_cr_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is only negative roles.
	path_cr_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.
	path_cr_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles
	path_cr_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanRevoke Block where the target role is also one of the goal roles
	path_cr_rules_target_goal	INTEGER ,
	-- The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.
	path_cr_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanRevoke Block that are startable, where a rule's precondition is TRUE or all negative roles.
	path_cr_rules_startable	INTEGER ,
	-- The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	path_cr_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	path_cr_rules_invokable	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.
	path_cr_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block
	path_cr_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanRevoke Block
	path_cr_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)
	path_cr_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)
	path_cr_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanRevoke Block that contain a pre-condition of length equal to 'path_cr_roles_longest_cond' 
	path_cr_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to 'path_cr_roles_longest_pos_cond' 
	path_cr_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to 'path_cr_timeslots_longest_tsarray'
	path_cr_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanEnable Block
	path_ce_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	path_ce_timeslots	INTEGER ,
	-- The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)
	path_ce_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanEnable Block
	path_ce_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanEnable Block
	path_ce_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanEnable Block
	path_ce_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanEnable Block
	path_ce_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanEnable Block
	path_ce_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	path_ce_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block
	path_ce_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanEnable Block
	path_ce_rules_admin_true	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is set to TRUE.
	path_ce_rules_precond_true	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is only positive roles.
	path_ce_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is only negative roles.
	path_ce_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.
	path_ce_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles
	path_ce_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanEnable Block where the target role is also one of the goal roles
	path_ce_rules_target_goal	INTEGER ,
	-- The number of rules in the CanEnable Block where the admin condition is one of the goal roles.
	path_ce_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanEnable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	path_ce_rules_startable	INTEGER ,
	-- The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	path_ce_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	path_ce_rules_invokable	INTEGER ,
	-- The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.
	path_ce_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanEnable Block
	path_ce_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanEnable Block
	path_ce_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)
	path_ce_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)
	path_ce_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanEnable Block that contain a pre-condition of length equal to 'path_ce_roles_longest_cond' 
	path_ce_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to 'path_ce_roles_longest_pos_cond' 
	path_ce_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to 'path_ce_timeslots_longest_tsarray'
	path_ce_longest_rules_tsarray	INTEGER ,
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
	-- The number of all rules in the CanDisable Block
	path_cd_rules	INTEGER ,
	-- The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	path_cd_timeslots	INTEGER ,
	-- The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)
	path_cd_roles	INTEGER ,
	--===================================
	-- The number of roles that appear in the admin condition of all rules in the CanDisable Block
	path_cd_roles_admin	INTEGER ,
	-- The number of roles that appear in the target role of all rules in the CanDisable Block
	path_cd_roles_target	INTEGER ,
	-- The number of roles that appear as positive conditions in all rules in the CanDisable Block
	path_cd_roles_pos_cond	INTEGER ,
	-- The number of roles that appear as negative conditions in all rules in the CanDisable Block
	path_cd_roles_neg_cond	INTEGER ,
	-- The number of roles that appear in the pre-condition in all rules in the CanDisable Block
	path_cd_roles_cond	INTEGER ,
	--===================================
	-- The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	path_cd_timeintervals_admin	INTEGER ,
	-- The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block
	path_cd_timeslots_target	INTEGER ,
	--===================================
	-- The number of rules that have TRUE for the admin condition in the CanDisable Block
	path_cd_rules_admin_true	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is set to TRUE.
	path_cd_rules_precond_true	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is only positive roles.
	path_cd_rules_precond_pos	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is only negative roles.
	path_cd_rules_precond_neg	INTEGER ,
	-- The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.
	path_cd_rules_precond_mixed	INTEGER ,
	-- The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles
	path_cd_rules_precond_goal	INTEGER ,
	-- The number of rules in the CanDisable Block where the target role is also one of the goal roles
	path_cd_rules_target_goal	INTEGER ,
	-- The number of rules in the CanDisable Block where the admin condition is one of the goal roles.
	path_cd_rules_admin_goal	INTEGER ,
	--===================================
	-- The number of rules in the CanDisable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	path_cd_rules_startable	INTEGER ,
	-- The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	path_cd_rules_truely_startable	INTEGER ,
	-- The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	path_cd_rules_invokable	INTEGER ,
	-- The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.
	path_cd_rules_unassignable_precond	INTEGER ,
	--===================================
	-- The largest number of time-slots in target time-slot array in all rules in the CanDisable Block
	path_cd_longest_timeslots_tsarray	INTEGER ,
	-- The number of roles in the longest pre-condition in all rules in the CanDisable Block
	path_cd_longest_roles_cond	INTEGER ,
	-- The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)
	path_cd_longest_roles_pos_cond	INTEGER ,
	-- The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)
	path_cd_longest_roles_neg_cond	INTEGER ,
	--===================================
	-- The number of rules in the CanDisable Block that contain a pre-condition of length equal to 'path_cd_roles_longest_cond' 
	path_cd_longest_rules_cond	INTEGER ,
	-- The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to 'path_cd_roles_longest_pos_cond' 
	path_cd_longest_rules_pos_cond	INTEGER ,
	-- The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to 'path_cd_timeslots_longest_tsarray'
	path_cd_longest_rules_tsarray	INTEGER 
	--------------------------------------------------------------------------------

	--------------------------------------------------------------------------------
)