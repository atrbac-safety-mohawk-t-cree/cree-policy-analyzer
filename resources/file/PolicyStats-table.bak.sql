CREATE TABLE PolicyStats
(
    -- Unique row id
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    -- When the policy analyzer is performing a BULK analysis on a folder of policy files, each new row will have the same batch_id.
    batch_id	INTEGER NOT NULL,
    -- The date that this row is created (set automatically upon inserting into the table)
    date_created	DATETIME NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
    -- This field is initialized to date_created, but if the policy editor updates unset values, it will update this field to indicate that the row has been updated.
    last_updated	DATETIME NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW')),
    -- The name of the program which entered this row into the database (can be retroactively updated).
    created_by	VARCHAR(80) NOT NULL,
    -- The policy file's name (does not include the file path). For in-memeory policies a special name can be used to indicate it.
    filename	VARCHAR(256),
    -- The policy file's parent folder name (does not include the file path). Useful for grouping similar policies. For in-memeory policies a special name can be used to indicate it.
    folder	VARCHAR(256),
    -- The complete folder path to the policy file. Can be NULL if the policy is in-memory.
    filepath	TEXT,
    -- The SHA256 of the file. In-memory policies can get teh text version and SHA256 that version.
    file_sha256	VARCHAR(257),
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- If the policy file comes with a "Expected" tag, then the result is stored here. The possible values are: 'R' (reachable), 'U'( unreachable), NULL (not present)
    result_expected	CHAR,
    -- The result returned by the program Cree. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)
    result_cree	CHAR,
    -- The date and time when this policy was run through the Cree program.
    result_cree_dt	DATETIME,
    -- The result returned by the program Mohawk+T. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)
    result_mohawk_t	CHAR,
    -- The date and time when this policy was run through the Mohawk+T program.
    result_mohawk_t_dt	DATETIME,
    -- The result returned by the program ASASPTime NSA. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)
    result_asasptime_nsa	CHAR,
    -- The date and time when this policy was run through the ASASPTime NSA program.
    result_asasptime_nsa_dt	DATETIME,
    -- The result returned by the program ASASPTime SA. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)
    result_asasptime_sa	CHAR,
    -- The date and time when this policy was run through the ASASPTime SA program.
    result_asasptime_sa_dt	DATETIME,
    -- The result returned by the program TREDRole. Possible values: 'R', 'U', 'E' (error), NULL (has not been run)
    result_uzun_tredrole	CHAR,
    -- The date and time when this policy was run through the TREDRole program.
    result_uzun_tredrole_dt	DATETIME,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The percentage of rules that are Can Assign rules compared to the total number of rules [0.0, 100.0]
    stat_ca_rules_percent	REAL,
    -- The percentage of rules that are Can Revoke rules compared to the total number of rules [0.0, 100.0]
    stat_cr_rules_percent	REAL,
    -- The percentage of rules that are Can Enable rules compared to the total number of rules [0.0, 100.0]
    stat_ce_rules_percent	REAL,
    -- The percentage of rules that are Can Disable rules compared to the total number of rules [0.0, 100.0]
    stat_cd_rules_percent	REAL,

    -- The average length of precondition for all rules in the policy
    stat_all_mean_cond  REAL,
    -- The average length of precondition for all Can Assign rules
    stat_ca_mean_cond  REAL,
    -- The average length of precondition for all Can Revoke rules
    stat_cr_mean_cond  REAL,
    -- The average length of precondition for all Can Enable rules
    stat_ce_mean_cond  REAL,
    -- The average length of precondition for all Can Disable rules
    stat_cd_mean_cond  REAL,

    -- The standard deviation (population) for the length of precondition for all rules in the policy
    stat_all_stddevp_cond  REAL,
    -- The standard deviation (population) for the length of precondition for all CA rules
    stat_ca_stddevp_cond  REAL,
    -- The standard deviation (population) for the length of precondition for all CR rules
    stat_cr_stddevp_cond  REAL,
    -- The standard deviation (population) for the length of precondition for all CE rules
    stat_ce_stddevp_cond  REAL,
    -- The standard deviation (population) for the length of precondition for all CD rules
    stat_cd_stddevp_cond  REAL,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The number of goal roles in the policy
    num_goal_roles	INTEGER,
    -- The number of all rules in the policy
    num_all_rules	INTEGER,
    -- The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
    num_all_timeslots	INTEGER,
    -- The number of roles used in the entire policy (counts over: goal, target, admin, and pre-condition roles)
    num_all_roles	INTEGER,
    --===================================
    -- The number of roles that appear in the admin condition of all rules in the policy
    num_all_roles_admin	INTEGER,
    -- The number of roles that appear as positive conditions in all rules in the policy
    num_all_roles_pos_cond	INTEGER,
    -- The number of roles that appear as negative conditions in all rules in the policy
    num_all_roles_neg_cond	INTEGER,
    -- The number of roles that appear in the pre-condition in all rules in the policy
    num_all_roles_cond	INTEGER,
    --===================================
    -- The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
    num_all_timeintervals_admin	INTEGER,
    -- The number of unique time slots that appear in the target time slot array for all rules in the policy
    num_all_timeslots_target	INTEGER,
    --===================================
    -- The number of rules that have TRUE for the admin condition in the whole policy
    num_all_rules_admin_true	INTEGER,
    -- The number of rules in the policy where the pre-condition is set to TRUE.
    num_all_rules_precond_true	INTEGER,
    -- The number of rules in the policy where the pre-condition is only positive roles.
    num_all_rules_precond_pos	INTEGER,
    -- The number of rules in the policy where the pre-condition is only negative roles.
    num_all_rules_precond_neg	INTEGER,
    -- The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
    num_all_rules_precond_mixed	INTEGER,
    -- The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
    num_all_rules_precond_goal	INTEGER,
    -- The number of rules in the policy where the target role is also one of the goal roles
    num_all_rules_target_goal	INTEGER,
    -- The number of rules in the policy where the admin condition is one of the goal roles.
    num_all_rules_admin_goal	INTEGER,
    --===================================
    -- The largest number of timeslots in target timeslot array in all rules in the policy
    num_all_longest_timeslots_tsarray	INTEGER,
    -- The number of roles in the longest pre-conditiion in all rules in the policy
    num_all_longest_roles_cond	INTEGER,
    -- The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
    num_all_longest_roles_pos_cond	INTEGER,
    -- The largest number of negative roles in pre-conditions in all rules in the policy (precondtiion can contain positive roles too)
    num_all_longest_roles_neg_cond	INTEGER,
    --===================================
    -- The number of rules in the policy that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
    num_all_rules_longest_cond	INTEGER,
    -- The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'num_all_roles_longest_pos_cond' 
    num_all_rules_longest_pos_cond	INTEGER,
    -- The number of rules in the policy that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' 
    num_all_rules_longest_tsarray	INTEGER,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The number of all rules in the Can Assign rule section
    num_ca_rules	INTEGER,
    -- The number of non-overlapping time-slots in the Can Assign rule section. Aligns time-slots and time-intervals and gets a list of non-overlapping 
    num_ca_timeslots	INTEGER,
    -- The number of roles used in the entire Can Assign rule section (counts over: goal, target, admin, and pre-condition roles)
    num_ca_roles	INTEGER,
    -- The number of roles that appear in the admin condition of all rules in the Can Assign rule section
    num_ca_roles_admin	INTEGER,
    -- The number of roles that appear in the pre-condition in all rules in the Can Assign rule section
    num_ca_roles_cond	INTEGER,
    -- The number of roles that appear as positive conditions in all rules in the Can Assign rule section
    num_ca_roles_pos_cond	INTEGER,
    -- The number of roles that appear as negative conditions in all rules in the Can Assign rule section
    num_ca_roles_neg_cond	INTEGER,
    -- The number of roles in the longest pre-conditiion in all rules in the Can Assign rule section
    num_ca_roles_longest_cond	INTEGER,
    -- The largest number of positive roles in pre-conditions in all rules in the Can Assign rule section
    num_ca_roles_longest_pos_cond	INTEGER,
    -- The largest number of negative roles in pre-conditions in all rules in the Can Assign rule section
    num_ca_roles_longest_neg_cond	INTEGER,
    --===================================
    -- The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
    num_ca_timeintervals_admin	INTEGER,
    -- The number of unique time slots that appear in the target time slot array for all rules in the Can Assign rule section
    num_ca_timeslots_target	INTEGER,
    -- The largest number of timeslots in target timeslot array in all rules in the Can Assign rule section
    num_ca_timeslots_longest_tsarray	INTEGER,
    --===================================
    -- The number of rules that have TRUE for the admin condition in the whole Can Assign rule section
    num_ca_rules_admin_true	INTEGER,
    -- The number of rules in the Can Assign rule section where the pre-condition is set to TRUE.
    num_ca_rules_precond_true	INTEGER,
    -- The number of rules in the Can Assign rule section where the target role is also one of the goal roles
    num_ca_rules_target_goal	INTEGER,
    -- The number of rules in the Can Assign rule section where one of the roles in the pre-condition is also one of the goal roles
    num_ca_rules_precond_goal	INTEGER,
    -- The number of rules in the Can Assign rule section where the admin condition is one of the goal roles.
    num_ca_rules_admin_goal	INTEGER,
    -- The number of rules in the Can Assign rule section that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
    num_ca_rules_longest_cond	INTEGER,
    -- The number of rules in the Can Assign rule section that contain the same number of positive roles in the pre-condition of llength equal to 'num_all_roles_longest_pos_cond' 
    num_ca_rules_longest_pos_cond	INTEGER,
    -- The number of rules in the Can Assign rule section that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' 
    num_ca_rules_longest_tsarray	INTEGER,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The number of all rules in the Can Revoke rule section
    num_cr_rules	INTEGER,
    -- The number of non-overlapping time-slots in the Can Revoke rule section. Aligns time-slots and time-intervals and gets a list of non-overlapping 
    num_cr_timeslots	INTEGER,
    -- The number of roles used in the entire Can Revoke rule section (counts over: goal, target, admin, and pre-condition roles)
    num_cr_roles	INTEGER,
    -- The number of roles that appear in the admin condition of all rules in the Can Revoke rule section
    num_cr_roles_admin	INTEGER,
    -- The number of roles that appear in the pre-condition in all rules in the Can Revoke rule section
    num_cr_roles_cond	INTEGER,
    -- The number of roles that appear as positive conditions in all rules in the Can Revoke rule section
    num_cr_roles_pos_cond	INTEGER,
    -- The number of roles that appear as negative conditions in all rules in the Can Revoke rule section
    num_cr_roles_neg_cond	INTEGER,
    -- The number of roles in the longest pre-conditiion in all rules in the Can Revoke rule section
    num_cr_roles_longest_cond	INTEGER,
    -- The largest number of positive roles in pre-conditions in all rules in the Can Revoke rule section
    num_cr_roles_longest_pos_cond	INTEGER,
    -- The largest number of negative roles in pre-conditions in all rules in the Can Revoke rule section
    num_cr_roles_longest_neg_cond	INTEGER,
    --===================================
    -- The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
    num_cr_timeintervals_admin	INTEGER,
    -- The number of unique time slots that appear in the target time slot array for all rules in the Can Revoke rule section
    num_cr_timeslots_target	INTEGER,
    -- The largest number of timeslots in target timeslot array in all rules in the Can Revoke rule section
    num_cr_timeslots_longest_tsarray	INTEGER,
    --===================================
    -- The number of rules that have TRUE for the admin condition in the whole Can Revoke rule section
    num_cr_rules_admin_true	INTEGER,
    -- The number of rules in the Can Revoke rule section where the pre-condition is set to TRUE.
    num_cr_rules_precond_true	INTEGER,
    -- The number of rules in the Can Revoke rule section where the target role is also one of the goal roles
    num_cr_rules_target_goal	INTEGER,
    -- The number of rules in the Can Revoke rule section where one of the roles in the pre-condition is also one of the goal roles
    num_cr_rules_precond_goal	INTEGER,
    -- The number of rules in the Can Revoke rule section where the admin condition is one of the goal roles.
    num_cr_rules_admin_goal	INTEGER,
    -- The number of rules in the Can Revoke rule section that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
    num_cr_rules_longest_cond	INTEGER,
    -- The number of rules in the Can Revoke rule section that contain the same number of positive roles in the pre-condition of llength equal to 'num_all_roles_longest_pos_cond' 
    num_cr_rules_longest_pos_cond	INTEGER,
    -- The number of rules in the Can Revoke rule section that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' 
    num_cr_rules_longest_tsarray	INTEGER,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The number of all rules in the Can Enable rule section
    num_ce_rules	INTEGER,
    -- The number of non-overlapping time-slots in the Can Enable rule section. Aligns time-slots and time-intervals and gets a list of non-overlapping 
    num_ce_timeslots	INTEGER,
    -- The number of roles used in the entire Can Enable rule section (counts over: goal, target, admin, and pre-condition roles)
    num_ce_roles	INTEGER,
    -- The number of roles that appear in the admin condition of all rules in the Can Enable rule section
    num_ce_roles_admin	INTEGER,
    -- The number of roles that appear in the pre-condition in all rules in the Can Enable rule section
    num_ce_roles_cond	INTEGER,
    -- The number of roles that appear as positive conditions in all rules in the Can Enable rule section
    num_ce_roles_pos_cond	INTEGER,
    -- The number of roles that appear as negative conditions in all rules in the Can Enable rule section
    num_ce_roles_neg_cond	INTEGER,
    -- The number of roles in the longest pre-conditiion in all rules in the Can Enable rule section
    num_ce_roles_longest_cond	INTEGER,
    -- The largest number of positive roles in pre-conditions in all rules in the Can Enable rule section
    num_ce_roles_longest_pos_cond	INTEGER,
    -- The largest number of negative roles in pre-conditions in all rules in the Can Enable rule section
    num_ce_roles_longest_neg_cond	INTEGER,
    --===================================
    -- The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
    num_ce_timeintervals_admin	INTEGER,
    -- The number of unique time slots that appear in the target time slot array for all rules in the Can Enable rule section
    num_ce_timeslots_target	INTEGER,
    -- The largest number of timeslots in target timeslot array in all rules in the Can Enable rule section
    num_ce_timeslots_longest_tsarray	INTEGER,
    --===================================
    -- The number of rules that have TRUE for the admin condition in the whole Can Enable rule section
    num_ce_rules_admin_true	INTEGER,
    -- The number of rules in the Can Enable rule section where the pre-condition is set to TRUE.
    num_ce_rules_precond_true	INTEGER,
    -- The number of rules in the Can Enable rule section where the target role is also one of the goal roles
    num_ce_rules_target_goal	INTEGER,
    -- The number of rules in the Can Enable rule section where one of the roles in the pre-condition is also one of the goal roles
    num_ce_rules_precond_goal	INTEGER,
    -- The number of rules in the Can Enable rule section where the admin condition is one of the goal roles.
    num_ce_rules_admin_goal	INTEGER,
    -- The number of rules in the Can Enable rule section that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
    num_ce_rules_longest_cond	INTEGER,
    -- The number of rules in the Can Enable rule section that contain the same number of positive roles in the pre-condition of llength equal to 'num_all_roles_longest_pos_cond' 
    num_ce_rules_longest_pos_cond	INTEGER,
    -- The number of rules in the Can Enable rule section that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' 
    num_ce_rules_longest_tsarray	INTEGER,
    ------------------------------------------------------------------------------

    ------------------------------------------------------------------------------
    -- The number of all rules in the Can Disable rule section
    num_cd_rules	INTEGER,
    -- The number of non-overlapping time-slots in the Can Disable rule section. Aligns time-slots and time-intervals and gets a list of non-overlapping 
    num_cd_timeslots	INTEGER,
    -- The number of roles used in the entire Can Disable rule section (counts over: goal, target, admin, and pre-condition roles)
    num_cd_roles	INTEGER,
    -- The number of roles that appear in the admin condition of all rules in the Can Disable rule section
    num_cd_roles_admin	INTEGER,
    -- The number of roles that appear in the pre-condition in all rules in the Can Disable rule section
    num_cd_roles_cond	INTEGER,
    -- The number of roles that appear as positive conditions in all rules in the Can Disable rule section
    num_cd_roles_pos_cond	INTEGER,
    -- The number of roles that appear as negative conditions in all rules in the Can Disable rule section
    num_cd_roles_neg_cond	INTEGER,
    -- The number of roles in the longest pre-conditiion in all rules in the Can Disable rule section
    num_cd_roles_longest_cond	INTEGER,
    -- The largest number of positive roles in pre-conditions in all rules in the Can Disable rule section
    num_cd_roles_longest_pos_cond	INTEGER,
    -- The largest number of negative roles in pre-conditions in all rules in the Can Disable rule section
    num_cd_roles_longest_neg_cond	INTEGER,
    --===================================
    -- The number of unique time-intervals supplied. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
    num_cd_timeintervals_admin	INTEGER,
    -- The number of unique time slots that appear in the target time slot array for all rules in the Can Disable rule section
    num_cd_timeslots_target	INTEGER,
    -- The largest number of timeslots in target timeslot array in all rules in the Can Disable rule section
    num_cd_timeslots_longest_tsarray	INTEGER,
    --===================================
    -- The number of rules that have TRUE for the admin condition in the whole Can Disable rule section
    num_cd_rules_admin_true	INTEGER,
    -- The number of rules in the Can Disable rule section where the pre-condition is set to TRUE.
    num_cd_rules_precond_true	INTEGER,
    -- The number of rules in the Can Disable rule section where the target role is also one of the goal roles
    num_cd_rules_target_goal	INTEGER,
    -- The number of rules in the Can Disable rule section where one of the roles in the pre-condition is also one of the goal roles
    num_cd_rules_precond_goal	INTEGER,
    -- The number of rules in the Can Disable rule section where the admin condition is one of the goal roles.
    num_cd_rules_admin_goal	INTEGER,
    -- The number of rules in the Can Disable rule section that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
    num_cd_rules_longest_cond	INTEGER,
    -- The number of rules in the Can Disable rule section that contain the same number of positive roles in the pre-condition of llength equal to 'num_all_roles_longest_pos_cond' 
    num_cd_rules_longest_pos_cond	INTEGER,
    -- The number of rules in the Can Disable rule section that contain the same number of roles in the timeslot array of length equal to 'num_all_timeslots_longest_tsarray' 
    num_cd_rules_longest_tsarray	INTEGER
    ------------------------------------------------------------------------------
);