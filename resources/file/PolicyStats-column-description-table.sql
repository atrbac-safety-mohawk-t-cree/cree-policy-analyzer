-- This file was automatically created by `generate_sql_java_class.py`
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: PolicyStats_Description
DROP TABLE IF EXISTS PolicyStats_Description;

CREATE TABLE PolicyStats_Description
(
    offset INTEGER,
    [column] VARCHAR (80) NOT NULL,
    data_type VARCHAR (50) NOT NULL,
    [description] TEXT NOT NULL
);
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('0', 'id', 'INTEGER', 'Unique row id');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('1', 'batch_id', 'INTEGER', 'When the policy analyzer is performing a BULK analysis on a folder of policy files, each new row will have the same batch_id.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('2', 'date_created', 'DATETIME', 'The date that this row is created (set automatically upon inserting into the table)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('3', 'last_updated', 'DATETIME', 'This field is initialized to date_created, but if the policy editor updates unset values, it will update this field to indicate that the row has been updated.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('4', 'created_by', 'VARCHAR(80)', 'The name of the program which entered this row into the database (can be retroactively updated).');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('5', 'filename', 'VARCHAR(256)', 'The policy file"s name (does not include the file path). For in-memory policies a special name can be used to indicate it.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('6', 'folder', 'VARCHAR(256)', 'The policy file"s parent folder name (does not include the file path). Useful for grouping similar policies. For in-memeory policies a special name can be used to indicate it.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('7', 'filepath', 'TEXT', 'The complete folder path to the policy file. Can be NULL if the policy is in-memory.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('8', 'file_sha256', 'VARCHAR(257)', 'The SHA256 of the file. In-memory policies can get the text version and SHA256 that version.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('9', 'owner', 'VARCHAR(256)', 'The creator of the policy file (which testsuite did it come from)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('10', 'complexity', 'VARCHAR(50)', 'This field indicates the theoretical complexity of the solver required to solve this policy file');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('11', 'alteration_method', 'TEXT', 'This field is either NULL, or some text that describes how it is modified from the original file');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('12', 'alteration_duration', 'INTEGER', 'This field holds the duration, in milliseconds, that the alteration method took. It is NULL if the alteration method is NULL');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('13', 'heuristics_result', 'TEXT', 'The result returned from running a polynomial Heuristic algorithm. NULL (not present)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('14', 'heuristics_duration', 'INTEGER', 'The time, in milliseconds, that it took to run the heuristic function on the policy.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('15', 'heuristics_description', 'TEXT', 'If the heuristic function returned "R" or "U", then this field contains the reason for how it knows that result');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('16', 'bound_estimation', 'INTEGER', 'Uses Cree"s Bound Estimation Algorithm');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('17', 'result_expected', 'TEXT', 'If the policy file comes with a "Expected" tag, then the result is stored here. NULL (not present)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('18', 'result_cree', 'TEXT', 'The result returned by the program Cree. NULL (has not been run)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('19', 'result_cree_dur', 'INTEGER', 'The duration in milliseconds of when this policy was run through the Cree program.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('20', 'result_mohawk_t', 'TEXT', 'The result returned by the program Mohawk+T. NULL (has not been run)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('21', 'result_mohawk_t_dur', 'INTEGER', 'The duration in milliseconds of when this policy was run through the Mohawk+T program.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('22', 'result_asasptime_nsa', 'TEXT', 'The result returned by the program ASASPTime NSA. NULL (has not been run)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('23', 'result_asasptime_nsa_dur', 'INTEGER', 'The duration in milliseconds of when this policy was run through the ASASPTime NSA program.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('24', 'result_asasptime_sa', 'TEXT', 'The result returned by the program ASASPTime SA. NULL (has not been run)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('25', 'result_asasptime_sa_dur', 'INTEGER', 'The duration in milliseconds of when this policy was run through the ASASPTime SA program.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('26', 'result_uzun_tredrole', 'TEXT', 'The result returned by the program TREDRole. NULL (has not been run)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('27', 'result_uzun_tredrole_dur', 'INTEGER', 'The duration in milliseconds of when this policy was run through the TREDRole program.');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('28', 'stat_size_max', 'INTEGER', 'The theoretical max size required to store the current state of the ATRBAC policy, as described by the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('29', 'stat_size_min', 'INTEGER', 'The theoretical min size required to store the current state of the ATRBAC policy, as described by the policy and by incorporating the bound estimation tightening techniques from the Cree journal paper (Shahen et.al.)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('30', 'stat_ca_rules_percent', 'REAL', 'The percentage of rules that are Can Assign rules compared to the total number of rules [0.0, 100.0]');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('31', 'stat_cr_rules_percent', 'REAL', 'The percentage of rules that are Can Revoke rules compared to the total number of rules [0.0, 100.0]');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('32', 'stat_ce_rules_percent', 'REAL', 'The percentage of rules that are Can Enable rules compared to the total number of rules [0.0, 100.0]');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('33', 'stat_cd_rules_percent', 'REAL', 'The percentage of rules that are Can Disable rules compared to the total number of rules [0.0, 100.0]');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('34', 'stat_all_mean_cond', 'REAL', 'The average length of precondition for all rules in the policy.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('35', 'stat_all_stddevp_cond', 'REAL', 'The standard deviation (population) for the length of precondition for all rules in the policy.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('36', 'stat_all_mean_tsarray', 'REAL', 'The average length of time-slot array for all rules in the policy.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('37', 'stat_all_stddevp_tsarray', 'REAL', 'The standard deviation (population) for the length of time-slot array for all rules in the policy.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('38', 'stat_ca_mean_cond', 'REAL', 'The average length of precondition for all rules in the CanAssign Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('39', 'stat_ca_stddevp_cond', 'REAL', 'The standard deviation (population) for the length of precondition for all rules in the CanAssign Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('40', 'stat_ca_mean_tsarray', 'REAL', 'The average length of time-slot array for all rules in the CanAssign Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('41', 'stat_ca_stddevp_tsarray', 'REAL', 'The standard deviation (population) for the length of time-slot array for all rules in the CanAssign Block.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('42', 'stat_cr_mean_cond', 'REAL', 'The average length of precondition for all rules in the CanRevoke Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('43', 'stat_cr_stddevp_cond', 'REAL', 'The standard deviation (population) for the length of precondition for all rules in the CanRevoke Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('44', 'stat_cr_mean_tsarray', 'REAL', 'The average length of time-slot array for all rules in the CanRevoke Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('45', 'stat_cr_stddevp_tsarray', 'REAL', 'The standard deviation (population) for the length of time-slot array for all rules in the CanRevoke Block.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('46', 'stat_ce_mean_cond', 'REAL', 'The average length of precondition for all rules in the CanEnable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('47', 'stat_ce_stddevp_cond', 'REAL', 'The standard deviation (population) for the length of precondition for all rules in the CanEnable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('48', 'stat_ce_mean_tsarray', 'REAL', 'The average length of time-slot array for all rules in the CanEnable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('49', 'stat_ce_stddevp_tsarray', 'REAL', 'The standard deviation (population) for the length of time-slot array for all rules in the CanEnable Block.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('50', 'stat_cd_mean_cond', 'REAL', 'The average length of precondition for all rules in the CanDisable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('51', 'stat_cd_stddevp_cond', 'REAL', 'The standard deviation (population) for the length of precondition for all rules in the CanDisable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('52', 'stat_cd_mean_tsarray', 'REAL', 'The average length of time-slot array for all rules in the CanDisable Block.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('53', 'stat_cd_stddevp_tsarray', 'REAL', 'The standard deviation (population) for the length of time-slot array for all rules in the CanDisable Block.');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('54', 'num_goal_roles', 'INTEGER', 'The number of goal roles in the policy');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('55', 'num_all_rules', 'INTEGER', 'The number of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('56', 'num_all_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('57', 'num_all_roles', 'INTEGER', 'The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('58', 'num_all_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('59', 'num_all_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('60', 'num_all_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('61', 'num_all_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('62', 'num_all_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the policy');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('63', 'num_all_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('64', 'num_all_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the policy');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('65', 'num_all_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('66', 'num_all_rules_precond_true', 'INTEGER', 'The number of rules in the policy where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('67', 'num_all_rules_precond_pos', 'INTEGER', 'The number of rules in the policy where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('68', 'num_all_rules_precond_neg', 'INTEGER', 'The number of rules in the policy where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('69', 'num_all_rules_precond_mixed', 'INTEGER', 'The number of rules in the policy where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('70', 'num_all_rules_precond_goal', 'INTEGER', 'The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('71', 'num_all_rules_target_goal', 'INTEGER', 'The number of rules in the policy where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('72', 'num_all_rules_admin_goal', 'INTEGER', 'The number of rules in the policy where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('73', 'num_all_rules_startable', 'INTEGER', 'The number of rules in the policy that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('74', 'num_all_rules_truely_startable', 'INTEGER', 'The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('75', 'num_all_rules_invokable', 'INTEGER', 'The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('76', 'num_all_rules_unassignable_precond', 'INTEGER', 'The number of rules in the policy that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('77', 'num_all_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('78', 'num_all_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('79', 'num_all_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('80', 'num_all_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('81', 'num_all_longest_rules_cond', 'INTEGER', 'The number of rules in the policy that contain a pre-condition of length equal to "num_all_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('82', 'num_all_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to "num_all_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('83', 'num_all_longest_rules_tsarray', 'INTEGER', 'The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to "num_all_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('84', 'num_ca_rules', 'INTEGER', 'The number of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('85', 'num_ca_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('86', 'num_ca_roles', 'INTEGER', 'The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('87', 'num_ca_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('88', 'num_ca_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('89', 'num_ca_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('90', 'num_ca_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('91', 'num_ca_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanAssign Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('92', 'num_ca_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('93', 'num_ca_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('94', 'num_ca_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('95', 'num_ca_rules_precond_true', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('96', 'num_ca_rules_precond_pos', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('97', 'num_ca_rules_precond_neg', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('98', 'num_ca_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('99', 'num_ca_rules_precond_goal', 'INTEGER', 'The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('100', 'num_ca_rules_target_goal', 'INTEGER', 'The number of rules in the CanAssign Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('101', 'num_ca_rules_admin_goal', 'INTEGER', 'The number of rules in the CanAssign Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('102', 'num_ca_rules_startable', 'INTEGER', 'The number of rules in the CanAssign Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('103', 'num_ca_rules_truely_startable', 'INTEGER', 'The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('104', 'num_ca_rules_invokable', 'INTEGER', 'The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('105', 'num_ca_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('106', 'num_ca_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('107', 'num_ca_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('108', 'num_ca_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('109', 'num_ca_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('110', 'num_ca_longest_rules_cond', 'INTEGER', 'The number of rules in the CanAssign Block that contain a pre-condition of length equal to "num_ca_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('111', 'num_ca_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to "num_ca_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('112', 'num_ca_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to "num_ca_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('113', 'num_cr_rules', 'INTEGER', 'The number of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('114', 'num_cr_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('115', 'num_cr_roles', 'INTEGER', 'The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('116', 'num_cr_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('117', 'num_cr_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('118', 'num_cr_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('119', 'num_cr_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('120', 'num_cr_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanRevoke Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('121', 'num_cr_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('122', 'num_cr_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('123', 'num_cr_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('124', 'num_cr_rules_precond_true', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('125', 'num_cr_rules_precond_pos', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('126', 'num_cr_rules_precond_neg', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('127', 'num_cr_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('128', 'num_cr_rules_precond_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('129', 'num_cr_rules_target_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('130', 'num_cr_rules_admin_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('131', 'num_cr_rules_startable', 'INTEGER', 'The number of rules in the CanRevoke Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('132', 'num_cr_rules_truely_startable', 'INTEGER', 'The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('133', 'num_cr_rules_invokable', 'INTEGER', 'The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('134', 'num_cr_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('135', 'num_cr_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('136', 'num_cr_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('137', 'num_cr_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('138', 'num_cr_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('139', 'num_cr_longest_rules_cond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain a pre-condition of length equal to "num_cr_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('140', 'num_cr_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to "num_cr_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('141', 'num_cr_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to "num_cr_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('142', 'num_ce_rules', 'INTEGER', 'The number of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('143', 'num_ce_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('144', 'num_ce_roles', 'INTEGER', 'The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('145', 'num_ce_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('146', 'num_ce_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('147', 'num_ce_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('148', 'num_ce_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('149', 'num_ce_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanEnable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('150', 'num_ce_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('151', 'num_ce_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('152', 'num_ce_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('153', 'num_ce_rules_precond_true', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('154', 'num_ce_rules_precond_pos', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('155', 'num_ce_rules_precond_neg', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('156', 'num_ce_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('157', 'num_ce_rules_precond_goal', 'INTEGER', 'The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('158', 'num_ce_rules_target_goal', 'INTEGER', 'The number of rules in the CanEnable Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('159', 'num_ce_rules_admin_goal', 'INTEGER', 'The number of rules in the CanEnable Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('160', 'num_ce_rules_startable', 'INTEGER', 'The number of rules in the CanEnable Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('161', 'num_ce_rules_truely_startable', 'INTEGER', 'The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('162', 'num_ce_rules_invokable', 'INTEGER', 'The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('163', 'num_ce_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('164', 'num_ce_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('165', 'num_ce_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('166', 'num_ce_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('167', 'num_ce_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('168', 'num_ce_longest_rules_cond', 'INTEGER', 'The number of rules in the CanEnable Block that contain a pre-condition of length equal to "num_ce_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('169', 'num_ce_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to "num_ce_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('170', 'num_ce_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to "num_ce_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('171', 'num_cd_rules', 'INTEGER', 'The number of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('172', 'num_cd_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('173', 'num_cd_roles', 'INTEGER', 'The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('174', 'num_cd_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('175', 'num_cd_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('176', 'num_cd_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('177', 'num_cd_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('178', 'num_cd_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanDisable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('179', 'num_cd_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('180', 'num_cd_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('181', 'num_cd_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('182', 'num_cd_rules_precond_true', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('183', 'num_cd_rules_precond_pos', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('184', 'num_cd_rules_precond_neg', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('185', 'num_cd_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('186', 'num_cd_rules_precond_goal', 'INTEGER', 'The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('187', 'num_cd_rules_target_goal', 'INTEGER', 'The number of rules in the CanDisable Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('188', 'num_cd_rules_admin_goal', 'INTEGER', 'The number of rules in the CanDisable Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('189', 'num_cd_rules_startable', 'INTEGER', 'The number of rules in the CanDisable Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('190', 'num_cd_rules_truely_startable', 'INTEGER', 'The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('191', 'num_cd_rules_invokable', 'INTEGER', 'The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('192', 'num_cd_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('193', 'num_cd_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('194', 'num_cd_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('195', 'num_cd_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('196', 'num_cd_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('197', 'num_cd_longest_rules_cond', 'INTEGER', 'The number of rules in the CanDisable Block that contain a pre-condition of length equal to "num_cd_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('198', 'num_cd_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to "num_cd_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('199', 'num_cd_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to "num_cd_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('200', 'path_raw', 'TEXT', 'This field contains the raw text output from Cree; each rule is separated by " -> ", the rule type is denoted in the comment preceding each rule. The counter example starts with "Initial State" and end at the "Goal State".');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('201', 'path_all_rules', 'INTEGER', 'The number of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('202', 'path_all_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('203', 'path_all_roles', 'INTEGER', 'The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('204', 'path_all_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('205', 'path_all_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('206', 'path_all_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('207', 'path_all_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('208', 'path_all_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the policy');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('209', 'path_all_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('210', 'path_all_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the policy');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('211', 'path_all_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('212', 'path_all_rules_precond_true', 'INTEGER', 'The number of rules in the policy where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('213', 'path_all_rules_precond_pos', 'INTEGER', 'The number of rules in the policy where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('214', 'path_all_rules_precond_neg', 'INTEGER', 'The number of rules in the policy where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('215', 'path_all_rules_precond_mixed', 'INTEGER', 'The number of rules in the policy where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('216', 'path_all_rules_precond_goal', 'INTEGER', 'The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('217', 'path_all_rules_target_goal', 'INTEGER', 'The number of rules in the policy where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('218', 'path_all_rules_admin_goal', 'INTEGER', 'The number of rules in the policy where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('219', 'path_all_rules_startable', 'INTEGER', 'The number of rules in the policy that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('220', 'path_all_rules_truely_startable', 'INTEGER', 'The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('221', 'path_all_rules_invokable', 'INTEGER', 'The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('222', 'path_all_rules_unassignable_precond', 'INTEGER', 'The number of rules in the policy that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('223', 'path_all_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('224', 'path_all_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the policy');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('225', 'path_all_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('226', 'path_all_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('227', 'path_all_longest_rules_cond', 'INTEGER', 'The number of rules in the policy that contain a pre-condition of length equal to "path_all_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('228', 'path_all_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to "path_all_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('229', 'path_all_longest_rules_tsarray', 'INTEGER', 'The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to "path_all_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('230', 'path_ca_rules', 'INTEGER', 'The number of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('231', 'path_ca_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('232', 'path_ca_roles', 'INTEGER', 'The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('233', 'path_ca_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('234', 'path_ca_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('235', 'path_ca_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('236', 'path_ca_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('237', 'path_ca_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanAssign Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('238', 'path_ca_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('239', 'path_ca_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('240', 'path_ca_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('241', 'path_ca_rules_precond_true', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('242', 'path_ca_rules_precond_pos', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('243', 'path_ca_rules_precond_neg', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('244', 'path_ca_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('245', 'path_ca_rules_precond_goal', 'INTEGER', 'The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('246', 'path_ca_rules_target_goal', 'INTEGER', 'The number of rules in the CanAssign Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('247', 'path_ca_rules_admin_goal', 'INTEGER', 'The number of rules in the CanAssign Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('248', 'path_ca_rules_startable', 'INTEGER', 'The number of rules in the CanAssign Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('249', 'path_ca_rules_truely_startable', 'INTEGER', 'The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('250', 'path_ca_rules_invokable', 'INTEGER', 'The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('251', 'path_ca_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('252', 'path_ca_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('253', 'path_ca_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanAssign Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('254', 'path_ca_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('255', 'path_ca_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('256', 'path_ca_longest_rules_cond', 'INTEGER', 'The number of rules in the CanAssign Block that contain a pre-condition of length equal to "path_ca_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('257', 'path_ca_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to "path_ca_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('258', 'path_ca_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to "path_ca_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('259', 'path_cr_rules', 'INTEGER', 'The number of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('260', 'path_cr_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('261', 'path_cr_roles', 'INTEGER', 'The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('262', 'path_cr_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('263', 'path_cr_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('264', 'path_cr_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('265', 'path_cr_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('266', 'path_cr_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanRevoke Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('267', 'path_cr_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('268', 'path_cr_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('269', 'path_cr_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('270', 'path_cr_rules_precond_true', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('271', 'path_cr_rules_precond_pos', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('272', 'path_cr_rules_precond_neg', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('273', 'path_cr_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('274', 'path_cr_rules_precond_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('275', 'path_cr_rules_target_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('276', 'path_cr_rules_admin_goal', 'INTEGER', 'The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('277', 'path_cr_rules_startable', 'INTEGER', 'The number of rules in the CanRevoke Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('278', 'path_cr_rules_truely_startable', 'INTEGER', 'The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('279', 'path_cr_rules_invokable', 'INTEGER', 'The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('280', 'path_cr_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('281', 'path_cr_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('282', 'path_cr_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanRevoke Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('283', 'path_cr_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('284', 'path_cr_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('285', 'path_cr_longest_rules_cond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain a pre-condition of length equal to "path_cr_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('286', 'path_cr_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to "path_cr_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('287', 'path_cr_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to "path_cr_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('288', 'path_ce_rules', 'INTEGER', 'The number of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('289', 'path_ce_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('290', 'path_ce_roles', 'INTEGER', 'The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('291', 'path_ce_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('292', 'path_ce_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('293', 'path_ce_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('294', 'path_ce_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('295', 'path_ce_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanEnable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('296', 'path_ce_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('297', 'path_ce_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('298', 'path_ce_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('299', 'path_ce_rules_precond_true', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('300', 'path_ce_rules_precond_pos', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('301', 'path_ce_rules_precond_neg', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('302', 'path_ce_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('303', 'path_ce_rules_precond_goal', 'INTEGER', 'The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('304', 'path_ce_rules_target_goal', 'INTEGER', 'The number of rules in the CanEnable Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('305', 'path_ce_rules_admin_goal', 'INTEGER', 'The number of rules in the CanEnable Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('306', 'path_ce_rules_startable', 'INTEGER', 'The number of rules in the CanEnable Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('307', 'path_ce_rules_truely_startable', 'INTEGER', 'The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('308', 'path_ce_rules_invokable', 'INTEGER', 'The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('309', 'path_ce_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('310', 'path_ce_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('311', 'path_ce_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanEnable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('312', 'path_ce_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('313', 'path_ce_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('314', 'path_ce_longest_rules_cond', 'INTEGER', 'The number of rules in the CanEnable Block that contain a pre-condition of length equal to "path_ce_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('315', 'path_ce_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to "path_ce_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('316', 'path_ce_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to "path_ce_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('317', 'path_cd_rules', 'INTEGER', 'The number of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('318', 'path_cd_timeslots', 'INTEGER', 'The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('319', 'path_cd_roles', 'INTEGER', 'The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('320', 'path_cd_roles_admin', 'INTEGER', 'The number of roles that appear in the admin condition of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('321', 'path_cd_roles_target', 'INTEGER', 'The number of roles that appear in the target role of all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('322', 'path_cd_roles_pos_cond', 'INTEGER', 'The number of roles that appear as positive conditions in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('323', 'path_cd_roles_neg_cond', 'INTEGER', 'The number of roles that appear as negative conditions in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('324', 'path_cd_roles_cond', 'INTEGER', 'The number of roles that appear in the pre-condition in all rules in the CanDisable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('325', 'path_cd_timeintervals_admin', 'INTEGER', 'The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('326', 'path_cd_timeslots_target', 'INTEGER', 'The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('327', 'path_cd_rules_admin_true', 'INTEGER', 'The number of rules that have TRUE for the admin condition in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('328', 'path_cd_rules_precond_true', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is set to TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('329', 'path_cd_rules_precond_pos', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is only positive roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('330', 'path_cd_rules_precond_neg', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is only negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('331', 'path_cd_rules_precond_mixed', 'INTEGER', 'The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('332', 'path_cd_rules_precond_goal', 'INTEGER', 'The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('333', 'path_cd_rules_target_goal', 'INTEGER', 'The number of rules in the CanDisable Block where the target role is also one of the goal roles');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('334', 'path_cd_rules_admin_goal', 'INTEGER', 'The number of rules in the CanDisable Block where the admin condition is one of the goal roles.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('335', 'path_cd_rules_startable', 'INTEGER', 'The number of rules in the CanDisable Block that are startable, where a rule"s precondition is TRUE or all negative roles.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('336', 'path_cd_rules_truely_startable', 'INTEGER', 'The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('337', 'path_cd_rules_invokable', 'INTEGER', 'The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('338', 'path_cd_rules_unassignable_precond', 'INTEGER', 'The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('339', 'path_cd_longest_timeslots_tsarray', 'INTEGER', 'The largest number of time-slots in target time-slot array in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('340', 'path_cd_longest_roles_cond', 'INTEGER', 'The number of roles in the longest pre-condition in all rules in the CanDisable Block');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('341', 'path_cd_longest_roles_pos_cond', 'INTEGER', 'The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('342', 'path_cd_longest_roles_neg_cond', 'INTEGER', 'The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','==========');
--===================================
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('343', 'path_cd_longest_rules_cond', 'INTEGER', 'The number of rules in the CanDisable Block that contain a pre-condition of length equal to "path_cd_roles_longest_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('344', 'path_cd_longest_rules_pos_cond', 'INTEGER', 'The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to "path_cd_roles_longest_pos_cond" ');
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('345', 'path_cd_longest_rules_tsarray', 'INTEGER', 'The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to "path_cd_timeslots_longest_tsarray"');
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
INSERT INTO PolicyStats_Description (offset,[column],data_type,[description]) VALUES('','','','================================================');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
