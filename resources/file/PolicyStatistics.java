// This file was automatically created by `generate_sql_java_class.py`
package cree.analyzer.results;

import java.lang.reflect.Field;
import java.sql.Timestamp;

/**
 * This class will store the Statistical results for a single policy file, 
 * it will then (in most cases) be written into a results file (most likely SQLite3). 
 * @author Jonathan Shahen
 *
 */
public class PolicyStatistics {
    ////////////////////////////////////////////////////////////////////////////////
    public PolicyStatistics(String policySHA256) {
        if (policySHA256 == null) { throw new IllegalArgumentException("policySHA256 cannot be NULL"); }
        file_sha256 = policySHA256;
    }
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// Unique row id
	public Integer id;
	// When the policy analyzer is performing a BULK analysis on a folder of policy files, each new row will have the same batch_id.
	public Integer batch_id;
	// The date that this row is created (set automatically upon inserting into the table)
	public Timestamp date_created;
	// This field is initialized to date_created, but if the policy editor updates unset values, it will update this field to indicate that the row has been updated.
	public Timestamp last_updated;
	// The name of the program which entered this row into the database (can be retroactively updated).
	public String created_by;
	// The policy file's name (does not include the file path). For in-memory policies a special name can be used to indicate it.
	public String filename;
	// The policy file's parent folder name (does not include the file path). Useful for grouping similar policies. For in-memeory policies a special name can be used to indicate it.
	public String folder;
	// The complete folder path to the policy file. Can be NULL if the policy is in-memory.
	public String filepath;
	// The SHA256 of the file. In-memory policies can get the text version and SHA256 that version.
	public String file_sha256;
	// The creator of the policy file (which testsuite did it come from)
	public String owner;
	// This field indicates the theoretical complexity of the solver required to solve this policy file
	public String complexity;
	// This field is either NULL, or some text that describes how it is modified from the original file
	public String alteration_method;
	// This field holds the duration, in milliseconds, that the alteration method took. It is NULL if the alteration method is NULL
	public Integer alteration_duration;
	// The result returned from running a polynomial Heuristic algorithm. NULL (not present)
	public String heuristics_result;
	// The time, in milliseconds, that it took to run the heuristic function on the policy.
	public Integer heuristics_duration;
	// If the heuristic function returned 'R' or 'U', then this field contains the reason for how it knows that result
	public String heuristics_description;
	// Uses Cree's Bound Estimation Algorithm
	public Integer bound_estimation;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// If the policy file comes with a 'Expected' tag, then the result is stored here. NULL (not present)
	public String result_expected;
	// The result returned by the program Cree. NULL (has not been run)
	public String result_cree;
	// The duration in milliseconds of when this policy was run through the Cree program.
	public Integer result_cree_dur;
	// The result returned by the program Mohawk+T. NULL (has not been run)
	public String result_mohawk_t;
	// The duration in milliseconds of when this policy was run through the Mohawk+T program.
	public Integer result_mohawk_t_dur;
	// The result returned by the program ASASPTime NSA. NULL (has not been run)
	public String result_asasptime_nsa;
	// The duration in milliseconds of when this policy was run through the ASASPTime NSA program.
	public Integer result_asasptime_nsa_dur;
	// The result returned by the program ASASPTime SA. NULL (has not been run)
	public String result_asasptime_sa;
	// The duration in milliseconds of when this policy was run through the ASASPTime SA program.
	public Integer result_asasptime_sa_dur;
	// The result returned by the program TREDRole. NULL (has not been run)
	public String result_uzun_tredrole;
	// The duration in milliseconds of when this policy was run through the TREDRole program.
	public Integer result_uzun_tredrole_dur;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The theoretical max size required to store the current state of the ATRBAC policy, as described by the policy
	public Integer stat_size_max;
	// The theoretical min size required to store the current state of the ATRBAC policy, as described by the policy and by incorporating the bound estimation tightening techniques from the Cree journal paper (Shahen et.al.)
	public Integer stat_size_min;
	// The percentage of rules that are Can Assign rules compared to the total number of rules [0.0, 100.0]
	public Double stat_ca_rules_percent;
	// The percentage of rules that are Can Revoke rules compared to the total number of rules [0.0, 100.0]
	public Double stat_cr_rules_percent;
	// The percentage of rules that are Can Enable rules compared to the total number of rules [0.0, 100.0]
	public Double stat_ce_rules_percent;
	// The percentage of rules that are Can Disable rules compared to the total number of rules [0.0, 100.0]
	public Double stat_cd_rules_percent;
	//===================================
	// The average length of precondition for all rules in the policy.
	public Double stat_all_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the policy.
	public Double stat_all_stddevp_cond;
	// The average length of time-slot array for all rules in the policy.
	public Double stat_all_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the policy.
	public Double stat_all_stddevp_tsarray;
	//===================================
	// The average length of precondition for all rules in the CanAssign Block.
	public Double stat_ca_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the CanAssign Block.
	public Double stat_ca_stddevp_cond;
	// The average length of time-slot array for all rules in the CanAssign Block.
	public Double stat_ca_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the CanAssign Block.
	public Double stat_ca_stddevp_tsarray;
	//===================================
	// The average length of precondition for all rules in the CanRevoke Block.
	public Double stat_cr_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the CanRevoke Block.
	public Double stat_cr_stddevp_cond;
	// The average length of time-slot array for all rules in the CanRevoke Block.
	public Double stat_cr_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the CanRevoke Block.
	public Double stat_cr_stddevp_tsarray;
	//===================================
	// The average length of precondition for all rules in the CanEnable Block.
	public Double stat_ce_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the CanEnable Block.
	public Double stat_ce_stddevp_cond;
	// The average length of time-slot array for all rules in the CanEnable Block.
	public Double stat_ce_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the CanEnable Block.
	public Double stat_ce_stddevp_tsarray;
	//===================================
	// The average length of precondition for all rules in the CanDisable Block.
	public Double stat_cd_mean_cond;
	// The standard deviation (population) for the length of precondition for all rules in the CanDisable Block.
	public Double stat_cd_stddevp_cond;
	// The average length of time-slot array for all rules in the CanDisable Block.
	public Double stat_cd_mean_tsarray;
	// The standard deviation (population) for the length of time-slot array for all rules in the CanDisable Block.
	public Double stat_cd_stddevp_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of goal roles in the policy
	public Integer num_goal_roles;
	//===================================
	// The number of all rules in the policy
	public Integer num_all_rules;
	// The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_all_timeslots;
	// The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_all_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the policy
	public Integer num_all_roles_admin;
	// The number of roles that appear in the target role of all rules in the policy
	public Integer num_all_roles_target;
	// The number of roles that appear as positive conditions in all rules in the policy
	public Integer num_all_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the policy
	public Integer num_all_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the policy
	public Integer num_all_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_all_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the policy
	public Integer num_all_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the policy
	public Integer num_all_rules_admin_true;
	// The number of rules in the policy where the pre-condition is set to TRUE.
	public Integer num_all_rules_precond_true;
	// The number of rules in the policy where the pre-condition is only positive roles.
	public Integer num_all_rules_precond_pos;
	// The number of rules in the policy where the pre-condition is only negative roles.
	public Integer num_all_rules_precond_neg;
	// The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
	public Integer num_all_rules_precond_mixed;
	// The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_all_rules_precond_goal;
	// The number of rules in the policy where the target role is also one of the goal roles
	public Integer num_all_rules_target_goal;
	// The number of rules in the policy where the admin condition is one of the goal roles.
	public Integer num_all_rules_admin_goal;
	//===================================
	// The number of rules in the policy that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_all_rules_startable;
	// The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_all_rules_truely_startable;
	// The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_all_rules_invokable;
	// The number of rules in the policy that contain a positive precondition that is unassignable.
	public Integer num_all_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the policy
	public Integer num_all_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the policy
	public Integer num_all_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
	public Integer num_all_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)
	public Integer num_all_longest_roles_neg_cond;
	//===================================
	// The number of rules in the policy that contain a pre-condition of length equal to 'num_all_roles_longest_cond' 
	public Integer num_all_longest_rules_cond;
	// The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'num_all_roles_longest_pos_cond' 
	public Integer num_all_longest_rules_pos_cond;
	// The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to 'num_all_timeslots_longest_tsarray'
	public Integer num_all_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanAssign Block
	public Integer num_ca_rules;
	// The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_ca_timeslots;
	// The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_ca_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanAssign Block
	public Integer num_ca_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanAssign Block
	public Integer num_ca_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanAssign Block
	public Integer num_ca_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanAssign Block
	public Integer num_ca_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanAssign Block
	public Integer num_ca_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_ca_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block
	public Integer num_ca_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanAssign Block
	public Integer num_ca_rules_admin_true;
	// The number of rules in the CanAssign Block where the pre-condition is set to TRUE.
	public Integer num_ca_rules_precond_true;
	// The number of rules in the CanAssign Block where the pre-condition is only positive roles.
	public Integer num_ca_rules_precond_pos;
	// The number of rules in the CanAssign Block where the pre-condition is only negative roles.
	public Integer num_ca_rules_precond_neg;
	// The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.
	public Integer num_ca_rules_precond_mixed;
	// The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_ca_rules_precond_goal;
	// The number of rules in the CanAssign Block where the target role is also one of the goal roles
	public Integer num_ca_rules_target_goal;
	// The number of rules in the CanAssign Block where the admin condition is one of the goal roles.
	public Integer num_ca_rules_admin_goal;
	//===================================
	// The number of rules in the CanAssign Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_ca_rules_startable;
	// The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_ca_rules_truely_startable;
	// The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_ca_rules_invokable;
	// The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.
	public Integer num_ca_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanAssign Block
	public Integer num_ca_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanAssign Block
	public Integer num_ca_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)
	public Integer num_ca_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)
	public Integer num_ca_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanAssign Block that contain a pre-condition of length equal to 'num_ca_roles_longest_cond' 
	public Integer num_ca_longest_rules_cond;
	// The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to 'num_ca_roles_longest_pos_cond' 
	public Integer num_ca_longest_rules_pos_cond;
	// The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to 'num_ca_timeslots_longest_tsarray'
	public Integer num_ca_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanRevoke Block
	public Integer num_cr_rules;
	// The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_cr_timeslots;
	// The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_cr_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanRevoke Block
	public Integer num_cr_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanRevoke Block
	public Integer num_cr_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanRevoke Block
	public Integer num_cr_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanRevoke Block
	public Integer num_cr_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanRevoke Block
	public Integer num_cr_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_cr_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block
	public Integer num_cr_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanRevoke Block
	public Integer num_cr_rules_admin_true;
	// The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.
	public Integer num_cr_rules_precond_true;
	// The number of rules in the CanRevoke Block where the pre-condition is only positive roles.
	public Integer num_cr_rules_precond_pos;
	// The number of rules in the CanRevoke Block where the pre-condition is only negative roles.
	public Integer num_cr_rules_precond_neg;
	// The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.
	public Integer num_cr_rules_precond_mixed;
	// The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_cr_rules_precond_goal;
	// The number of rules in the CanRevoke Block where the target role is also one of the goal roles
	public Integer num_cr_rules_target_goal;
	// The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.
	public Integer num_cr_rules_admin_goal;
	//===================================
	// The number of rules in the CanRevoke Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_cr_rules_startable;
	// The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_cr_rules_truely_startable;
	// The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_cr_rules_invokable;
	// The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.
	public Integer num_cr_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block
	public Integer num_cr_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanRevoke Block
	public Integer num_cr_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)
	public Integer num_cr_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)
	public Integer num_cr_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanRevoke Block that contain a pre-condition of length equal to 'num_cr_roles_longest_cond' 
	public Integer num_cr_longest_rules_cond;
	// The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to 'num_cr_roles_longest_pos_cond' 
	public Integer num_cr_longest_rules_pos_cond;
	// The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to 'num_cr_timeslots_longest_tsarray'
	public Integer num_cr_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanEnable Block
	public Integer num_ce_rules;
	// The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_ce_timeslots;
	// The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_ce_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanEnable Block
	public Integer num_ce_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanEnable Block
	public Integer num_ce_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanEnable Block
	public Integer num_ce_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanEnable Block
	public Integer num_ce_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanEnable Block
	public Integer num_ce_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_ce_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block
	public Integer num_ce_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanEnable Block
	public Integer num_ce_rules_admin_true;
	// The number of rules in the CanEnable Block where the pre-condition is set to TRUE.
	public Integer num_ce_rules_precond_true;
	// The number of rules in the CanEnable Block where the pre-condition is only positive roles.
	public Integer num_ce_rules_precond_pos;
	// The number of rules in the CanEnable Block where the pre-condition is only negative roles.
	public Integer num_ce_rules_precond_neg;
	// The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.
	public Integer num_ce_rules_precond_mixed;
	// The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_ce_rules_precond_goal;
	// The number of rules in the CanEnable Block where the target role is also one of the goal roles
	public Integer num_ce_rules_target_goal;
	// The number of rules in the CanEnable Block where the admin condition is one of the goal roles.
	public Integer num_ce_rules_admin_goal;
	//===================================
	// The number of rules in the CanEnable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_ce_rules_startable;
	// The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_ce_rules_truely_startable;
	// The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_ce_rules_invokable;
	// The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.
	public Integer num_ce_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanEnable Block
	public Integer num_ce_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanEnable Block
	public Integer num_ce_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)
	public Integer num_ce_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)
	public Integer num_ce_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanEnable Block that contain a pre-condition of length equal to 'num_ce_roles_longest_cond' 
	public Integer num_ce_longest_rules_cond;
	// The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to 'num_ce_roles_longest_pos_cond' 
	public Integer num_ce_longest_rules_pos_cond;
	// The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to 'num_ce_timeslots_longest_tsarray'
	public Integer num_ce_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanDisable Block
	public Integer num_cd_rules;
	// The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer num_cd_timeslots;
	// The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer num_cd_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanDisable Block
	public Integer num_cd_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanDisable Block
	public Integer num_cd_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanDisable Block
	public Integer num_cd_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanDisable Block
	public Integer num_cd_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanDisable Block
	public Integer num_cd_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer num_cd_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block
	public Integer num_cd_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanDisable Block
	public Integer num_cd_rules_admin_true;
	// The number of rules in the CanDisable Block where the pre-condition is set to TRUE.
	public Integer num_cd_rules_precond_true;
	// The number of rules in the CanDisable Block where the pre-condition is only positive roles.
	public Integer num_cd_rules_precond_pos;
	// The number of rules in the CanDisable Block where the pre-condition is only negative roles.
	public Integer num_cd_rules_precond_neg;
	// The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.
	public Integer num_cd_rules_precond_mixed;
	// The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer num_cd_rules_precond_goal;
	// The number of rules in the CanDisable Block where the target role is also one of the goal roles
	public Integer num_cd_rules_target_goal;
	// The number of rules in the CanDisable Block where the admin condition is one of the goal roles.
	public Integer num_cd_rules_admin_goal;
	//===================================
	// The number of rules in the CanDisable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer num_cd_rules_startable;
	// The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer num_cd_rules_truely_startable;
	// The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer num_cd_rules_invokable;
	// The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.
	public Integer num_cd_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanDisable Block
	public Integer num_cd_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanDisable Block
	public Integer num_cd_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)
	public Integer num_cd_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)
	public Integer num_cd_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanDisable Block that contain a pre-condition of length equal to 'num_cd_roles_longest_cond' 
	public Integer num_cd_longest_rules_cond;
	// The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to 'num_cd_roles_longest_pos_cond' 
	public Integer num_cd_longest_rules_pos_cond;
	// The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to 'num_cd_timeslots_longest_tsarray'
	public Integer num_cd_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// This field contains the raw text output from Cree; each rule is separated by ' -> ', the rule type is denoted in the comment preceding each rule. The counter example starts with 'Initial State' and end at the 'Goal State'.
	public String path_raw;
	//===================================
	// The number of all rules in the policy
	public Integer path_all_rules;
	// The number of non-overlapping time-slots in the policy. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer path_all_timeslots;
	// The number of roles used in the policy (counts over: goal, target, admin, and pre-condition roles)
	public Integer path_all_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the policy
	public Integer path_all_roles_admin;
	// The number of roles that appear in the target role of all rules in the policy
	public Integer path_all_roles_target;
	// The number of roles that appear as positive conditions in all rules in the policy
	public Integer path_all_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the policy
	public Integer path_all_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the policy
	public Integer path_all_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the policy. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer path_all_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the policy
	public Integer path_all_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the policy
	public Integer path_all_rules_admin_true;
	// The number of rules in the policy where the pre-condition is set to TRUE.
	public Integer path_all_rules_precond_true;
	// The number of rules in the policy where the pre-condition is only positive roles.
	public Integer path_all_rules_precond_pos;
	// The number of rules in the policy where the pre-condition is only negative roles.
	public Integer path_all_rules_precond_neg;
	// The number of rules in the policy where the pre-condition is a mix of positive/negative roles.
	public Integer path_all_rules_precond_mixed;
	// The number of rules in the policy where one of the roles in the pre-condition is also one of the goal roles
	public Integer path_all_rules_precond_goal;
	// The number of rules in the policy where the target role is also one of the goal roles
	public Integer path_all_rules_target_goal;
	// The number of rules in the policy where the admin condition is one of the goal roles.
	public Integer path_all_rules_admin_goal;
	//===================================
	// The number of rules in the policy that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer path_all_rules_startable;
	// The number of rules in the policy that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer path_all_rules_truely_startable;
	// The number of rules in the policy that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer path_all_rules_invokable;
	// The number of rules in the policy that contain a positive precondition that is unassignable.
	public Integer path_all_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the policy
	public Integer path_all_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the policy
	public Integer path_all_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the policy (precondition can contain negative roles too)
	public Integer path_all_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the policy (precondition can contain positive roles too)
	public Integer path_all_longest_roles_neg_cond;
	//===================================
	// The number of rules in the policy that contain a pre-condition of length equal to 'path_all_roles_longest_cond' 
	public Integer path_all_longest_rules_cond;
	// The number of rules in the policy that contain the same number of positive roles in the pre-condition of length equal to 'path_all_roles_longest_pos_cond' 
	public Integer path_all_longest_rules_pos_cond;
	// The number of rules in the policy that contain the same number of roles in the time-slot array of length equal to 'path_all_timeslots_longest_tsarray'
	public Integer path_all_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanAssign Block
	public Integer path_ca_rules;
	// The number of non-overlapping time-slots in the CanAssign Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer path_ca_timeslots;
	// The number of roles used in the CanAssign Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer path_ca_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanAssign Block
	public Integer path_ca_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanAssign Block
	public Integer path_ca_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanAssign Block
	public Integer path_ca_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanAssign Block
	public Integer path_ca_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanAssign Block
	public Integer path_ca_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanAssign Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer path_ca_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanAssign Block
	public Integer path_ca_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanAssign Block
	public Integer path_ca_rules_admin_true;
	// The number of rules in the CanAssign Block where the pre-condition is set to TRUE.
	public Integer path_ca_rules_precond_true;
	// The number of rules in the CanAssign Block where the pre-condition is only positive roles.
	public Integer path_ca_rules_precond_pos;
	// The number of rules in the CanAssign Block where the pre-condition is only negative roles.
	public Integer path_ca_rules_precond_neg;
	// The number of rules in the CanAssign Block where the pre-condition is a mix of positive/negative roles.
	public Integer path_ca_rules_precond_mixed;
	// The number of rules in the CanAssign Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer path_ca_rules_precond_goal;
	// The number of rules in the CanAssign Block where the target role is also one of the goal roles
	public Integer path_ca_rules_target_goal;
	// The number of rules in the CanAssign Block where the admin condition is one of the goal roles.
	public Integer path_ca_rules_admin_goal;
	//===================================
	// The number of rules in the CanAssign Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer path_ca_rules_startable;
	// The number of rules in the CanAssign Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer path_ca_rules_truely_startable;
	// The number of rules in the CanAssign Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer path_ca_rules_invokable;
	// The number of rules in the CanAssign Block that contain a positive precondition that is unassignable.
	public Integer path_ca_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanAssign Block
	public Integer path_ca_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanAssign Block
	public Integer path_ca_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanAssign Block (precondition can contain negative roles too)
	public Integer path_ca_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanAssign Block (precondition can contain positive roles too)
	public Integer path_ca_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanAssign Block that contain a pre-condition of length equal to 'path_ca_roles_longest_cond' 
	public Integer path_ca_longest_rules_cond;
	// The number of rules in the CanAssign Block that contain the same number of positive roles in the pre-condition of length equal to 'path_ca_roles_longest_pos_cond' 
	public Integer path_ca_longest_rules_pos_cond;
	// The number of rules in the CanAssign Block that contain the same number of roles in the time-slot array of length equal to 'path_ca_timeslots_longest_tsarray'
	public Integer path_ca_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanRevoke Block
	public Integer path_cr_rules;
	// The number of non-overlapping time-slots in the CanRevoke Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer path_cr_timeslots;
	// The number of roles used in the CanRevoke Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer path_cr_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanRevoke Block
	public Integer path_cr_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanRevoke Block
	public Integer path_cr_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanRevoke Block
	public Integer path_cr_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanRevoke Block
	public Integer path_cr_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanRevoke Block
	public Integer path_cr_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanRevoke Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer path_cr_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanRevoke Block
	public Integer path_cr_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanRevoke Block
	public Integer path_cr_rules_admin_true;
	// The number of rules in the CanRevoke Block where the pre-condition is set to TRUE.
	public Integer path_cr_rules_precond_true;
	// The number of rules in the CanRevoke Block where the pre-condition is only positive roles.
	public Integer path_cr_rules_precond_pos;
	// The number of rules in the CanRevoke Block where the pre-condition is only negative roles.
	public Integer path_cr_rules_precond_neg;
	// The number of rules in the CanRevoke Block where the pre-condition is a mix of positive/negative roles.
	public Integer path_cr_rules_precond_mixed;
	// The number of rules in the CanRevoke Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer path_cr_rules_precond_goal;
	// The number of rules in the CanRevoke Block where the target role is also one of the goal roles
	public Integer path_cr_rules_target_goal;
	// The number of rules in the CanRevoke Block where the admin condition is one of the goal roles.
	public Integer path_cr_rules_admin_goal;
	//===================================
	// The number of rules in the CanRevoke Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer path_cr_rules_startable;
	// The number of rules in the CanRevoke Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer path_cr_rules_truely_startable;
	// The number of rules in the CanRevoke Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer path_cr_rules_invokable;
	// The number of rules in the CanRevoke Block that contain a positive precondition that is unassignable.
	public Integer path_cr_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanRevoke Block
	public Integer path_cr_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanRevoke Block
	public Integer path_cr_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain negative roles too)
	public Integer path_cr_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanRevoke Block (precondition can contain positive roles too)
	public Integer path_cr_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanRevoke Block that contain a pre-condition of length equal to 'path_cr_roles_longest_cond' 
	public Integer path_cr_longest_rules_cond;
	// The number of rules in the CanRevoke Block that contain the same number of positive roles in the pre-condition of length equal to 'path_cr_roles_longest_pos_cond' 
	public Integer path_cr_longest_rules_pos_cond;
	// The number of rules in the CanRevoke Block that contain the same number of roles in the time-slot array of length equal to 'path_cr_timeslots_longest_tsarray'
	public Integer path_cr_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanEnable Block
	public Integer path_ce_rules;
	// The number of non-overlapping time-slots in the CanEnable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer path_ce_timeslots;
	// The number of roles used in the CanEnable Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer path_ce_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanEnable Block
	public Integer path_ce_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanEnable Block
	public Integer path_ce_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanEnable Block
	public Integer path_ce_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanEnable Block
	public Integer path_ce_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanEnable Block
	public Integer path_ce_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanEnable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer path_ce_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanEnable Block
	public Integer path_ce_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanEnable Block
	public Integer path_ce_rules_admin_true;
	// The number of rules in the CanEnable Block where the pre-condition is set to TRUE.
	public Integer path_ce_rules_precond_true;
	// The number of rules in the CanEnable Block where the pre-condition is only positive roles.
	public Integer path_ce_rules_precond_pos;
	// The number of rules in the CanEnable Block where the pre-condition is only negative roles.
	public Integer path_ce_rules_precond_neg;
	// The number of rules in the CanEnable Block where the pre-condition is a mix of positive/negative roles.
	public Integer path_ce_rules_precond_mixed;
	// The number of rules in the CanEnable Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer path_ce_rules_precond_goal;
	// The number of rules in the CanEnable Block where the target role is also one of the goal roles
	public Integer path_ce_rules_target_goal;
	// The number of rules in the CanEnable Block where the admin condition is one of the goal roles.
	public Integer path_ce_rules_admin_goal;
	//===================================
	// The number of rules in the CanEnable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer path_ce_rules_startable;
	// The number of rules in the CanEnable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer path_ce_rules_truely_startable;
	// The number of rules in the CanEnable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer path_ce_rules_invokable;
	// The number of rules in the CanEnable Block that contain a positive precondition that is unassignable.
	public Integer path_ce_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanEnable Block
	public Integer path_ce_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanEnable Block
	public Integer path_ce_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanEnable Block (precondition can contain negative roles too)
	public Integer path_ce_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanEnable Block (precondition can contain positive roles too)
	public Integer path_ce_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanEnable Block that contain a pre-condition of length equal to 'path_ce_roles_longest_cond' 
	public Integer path_ce_longest_rules_cond;
	// The number of rules in the CanEnable Block that contain the same number of positive roles in the pre-condition of length equal to 'path_ce_roles_longest_pos_cond' 
	public Integer path_ce_longest_rules_pos_cond;
	// The number of rules in the CanEnable Block that contain the same number of roles in the time-slot array of length equal to 'path_ce_timeslots_longest_tsarray'
	public Integer path_ce_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// The number of all rules in the CanDisable Block
	public Integer path_cd_rules;
	// The number of non-overlapping time-slots in the CanDisable Block. Aligns time-slots and time-intervals and gets a list of non-overlapping 
	public Integer path_cd_timeslots;
	// The number of roles used in the CanDisable Block (counts over: goal, target, admin, and pre-condition roles)
	public Integer path_cd_roles;
	//===================================
	// The number of roles that appear in the admin condition of all rules in the CanDisable Block
	public Integer path_cd_roles_admin;
	// The number of roles that appear in the target role of all rules in the CanDisable Block
	public Integer path_cd_roles_target;
	// The number of roles that appear as positive conditions in all rules in the CanDisable Block
	public Integer path_cd_roles_pos_cond;
	// The number of roles that appear as negative conditions in all rules in the CanDisable Block
	public Integer path_cd_roles_neg_cond;
	// The number of roles that appear in the pre-condition in all rules in the CanDisable Block
	public Integer path_cd_roles_cond;
	//===================================
	// The number of unique time-intervals supplied in the CanDisable Block. Example: t1-t2, t2-t2, t1-t3, t1-t2; this would be 3 time-intervals
	public Integer path_cd_timeintervals_admin;
	// The number of unique time slots that appear in the target time slot array for all rules in the CanDisable Block
	public Integer path_cd_timeslots_target;
	//===================================
	// The number of rules that have TRUE for the admin condition in the CanDisable Block
	public Integer path_cd_rules_admin_true;
	// The number of rules in the CanDisable Block where the pre-condition is set to TRUE.
	public Integer path_cd_rules_precond_true;
	// The number of rules in the CanDisable Block where the pre-condition is only positive roles.
	public Integer path_cd_rules_precond_pos;
	// The number of rules in the CanDisable Block where the pre-condition is only negative roles.
	public Integer path_cd_rules_precond_neg;
	// The number of rules in the CanDisable Block where the pre-condition is a mix of positive/negative roles.
	public Integer path_cd_rules_precond_mixed;
	// The number of rules in the CanDisable Block where one of the roles in the pre-condition is also one of the goal roles
	public Integer path_cd_rules_precond_goal;
	// The number of rules in the CanDisable Block where the target role is also one of the goal roles
	public Integer path_cd_rules_target_goal;
	// The number of rules in the CanDisable Block where the admin condition is one of the goal roles.
	public Integer path_cd_rules_admin_goal;
	//===================================
	// The number of rules in the CanDisable Block that are startable, where a rule's precondition is TRUE or all negative roles.
	public Integer path_cd_rules_startable;
	// The number of rules in the CanDisable Block that are truely startable, where a rule is startable and the admin condition is TRUE.
	public Integer path_cd_rules_truely_startable;
	// The number of rules in the CanDisable Block that are invokable, where a rules precondition does not contain a role both positively and negatively."
	public Integer path_cd_rules_invokable;
	// The number of rules in the CanDisable Block that contain a positive precondition that is unassignable.
	public Integer path_cd_rules_unassignable_precond;
	//===================================
	// The largest number of time-slots in target time-slot array in all rules in the CanDisable Block
	public Integer path_cd_longest_timeslots_tsarray;
	// The number of roles in the longest pre-condition in all rules in the CanDisable Block
	public Integer path_cd_longest_roles_cond;
	// The largest number of positive roles in pre-conditions in all rules in the CanDisable Block (precondition can contain negative roles too)
	public Integer path_cd_longest_roles_pos_cond;
	// The largest number of negative roles in pre-conditions in all rules in the CanDisable Block (precondition can contain positive roles too)
	public Integer path_cd_longest_roles_neg_cond;
	//===================================
	// The number of rules in the CanDisable Block that contain a pre-condition of length equal to 'path_cd_roles_longest_cond' 
	public Integer path_cd_longest_rules_cond;
	// The number of rules in the CanDisable Block that contain the same number of positive roles in the pre-condition of length equal to 'path_cd_roles_longest_pos_cond' 
	public Integer path_cd_longest_rules_pos_cond;
	// The number of rules in the CanDisable Block that contain the same number of roles in the time-slot array of length equal to 'path_cd_timeslots_longest_tsarray'
	public Integer path_cd_longest_rules_tsarray;
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @param psb The PolicyStatisticsBlock object that is full of values
     * @param prefix Can be either: 'num', 'path'
     * @param key The block, can be either: 'all', 'ca', 'cr', 'ce', 'cd' 
     */
    public void importBlock(PolicyStatisticsBlock psb, String prefix, String key) {
        Class<? extends PolicyStatistics> c = this.getClass();
        Field ps_f;
        for (Field psb_f : psb.getClass().getDeclaredFields()) {
            try {
                String name = psb_f.getName();

                name = name.replace("num_", prefix + "_");
                if (!key.equals("all")) {
                    name = name.replace(prefix + "_all_", prefix + "_" + key + "_");
                    if (prefix.equals("num")) {
                        name = name.replace("stat_all_", "stat_" + key + "_");
                    }
                }
                ps_f = c.getDeclaredField(name);
                if (psb_f.get(psb) != null) {
                    ps_f.set(this, psb_f.get(psb));
                }
            } catch (Exception e) {}
        }
    }
    ////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PolicyStatistics {");

        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (field.get(this) != null) {
                    sb.append(field.getName()).append("=").append(field.get(this)).append(", ");
                }
            } catch (Exception e) {}
        }
        sb.append("}");
        return sb.toString();
    }
}
